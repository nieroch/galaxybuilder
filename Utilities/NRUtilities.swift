//
//  NRUtilities.swift
//  SpriteKitAccelerometerTutorial
//
//  Created by Niels Rasmussen on 7/10/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation

//#if DEBUG
    
    func Qalog(format : String = "", arguments: [CVarArgType]? = nil, function : String = #function, line : Int = #line) {
        //        let message = function + "[\(line)]: " + format
        var message = function + String(format: "[#%04d]", line)
        if let args = arguments {
            message = message + String(format: format, arguments: args)
        } else {
            message = message + format
        }
        print(message)
    }
    
//#else
    
//    func Qalog(format : String = "", arguments: [CVarArgType]? = nil, function : String = #function, line : Int = #line) {}
    
//#endif




