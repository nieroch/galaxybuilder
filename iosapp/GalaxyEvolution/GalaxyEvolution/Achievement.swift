//
//  Achievement.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/10/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Achievement object
open class Achievement {
    open var title : String
    open var image : UIImage
    open var description = ""
    open var descriptionTitle = ""
    open var state : String

    public init (name: String, image: UIImage, description: String, descriptionTitle: String, state: String) {
        self.title = name
        self.image = image
        self.description = description
        self.descriptionTitle = descriptionTitle
        self.state = state
    }

}
