//
//  StarView.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/12/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import UIKit

// Class for the animated star backgrounds. Don't think it is being used.
class StarView: UIView {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    var lineWidth : CGFloat = 2.0
    var alphaValue : CGFloat = 1.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(lineWidth)
        context?.setStrokeColor(UIColor.init(white: 1.0, alpha: alphaValue).cgColor)
        
        context?.move(to: CGPoint(x: 0, y: 0))
        context?.addLine(to: CGPoint(x: 0, y: 0.1))
        context?.addLine(to: CGPoint(x: 0.1, y: 0.1))
        context?.addLine(to: CGPoint(x: 0.1, y: 0))
        context?.addLine(to: CGPoint(x: 0, y: 0))
        
        context?.setFillColor(UIColor.init(white: 1.0, alpha: alphaValue).cgColor)
        context?.drawPath(using: CGPathDrawingMode.fillStroke)
    }
    
    func setStrokeFillOpacity(_ opacity: CGFloat) {
        alphaValue = opacity
    }

}
