//
//  GameViewController.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/7/16.
//  Copyright (c) 2016 Niels Rasmussen. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

// View controller for the game view that sets up the positions of labels and sliders and buttons. Also sets up views that appear when pausing the game or when a merger occurs.
open class GameViewController: UIViewController {

    @IBOutlet weak var gasLevel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var ageNumberLabel: UILabel!
    @IBOutlet weak var dilationNumberLabel: UILabel!
    @IBOutlet weak var universeAgeBar: UIProgressView!
    @IBOutlet weak var ageDilationSlider: UISlider!
    @IBOutlet weak var ageDilationLabel: UILabel!
    @IBOutlet weak var gasAccretionRateSlider: UISlider!
    @IBOutlet weak var gasAccretionRateLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var userType: UILabel!
    @IBOutlet weak var stellarMassLabel: UILabel!
    @IBOutlet weak var stellarMass: UILabel!
    @IBOutlet weak var totalMassLabel: UILabel!
    @IBOutlet weak var totalMass: UILabel!
    @IBOutlet weak var zoomInButton: UIButton!
    @IBOutlet weak var zoomOutButton: UIButton!
    //@IBOutlet weak var resumeButton: UIButton!

    let scene = GameScene(fileNamed: "UniverseScene")!
    var motionMangaer = CMMotionManager()
    fileprivate var skView : SKView?

    var overlayView: UIView!
    var alertView: UIView!
    var collisionAlertView: UIView!
    var decisionTreeView: UIView!
    var introView: UIView!
    var animator: UIDynamicAnimator!
    var attachmentBehavior : UIAttachmentBehavior!
    var snapBehavior : UISnapBehavior!
    var introCounter = 1

    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the view.
        self.universeAgeBar.progress = 0.0
        self.ageDilationSlider.value = -2550
        Universe.universe.updateSecondEquivalence(Double(self.ageDilationSlider.value))
        var slider = UIImage(named: "Slider")
        let size = CGSize(width: slider!.size.width * 0.05, height: slider!.size.height * 0.05)
        slider = self.imageWithImage(slider!, scaledToSize: size)
        self.ageDilationSlider.setThumbImage(slider, for: UIControlState())
        self.gasAccretionRateSlider.setThumbImage(slider, for: UIControlState())
        //self.ageDilationSlider.addTarget(self, action: #selector(GameViewController.updateAgeDilation), forControlEvents: UIControlEvents.)

        // Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.updateAgeProgress(_:)), name: NSNotification.Name(GENotification.Universe.AgeUpdated), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.updateGalaxy(_:)), name: NSNotification.Name(GENotification.Universe.SpaceObjectsUpdated), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.updateGalaxy(_:)), name: NSNotification.Name(GENotification.Galaxy.GalaxyMerged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.displayGasRichDiskCollisionView(_:)), name: NSNotification.Name(GENotification.Galaxy.MergedWithGasRichDisk), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.displayGasPoorDiskCollisionView(_:)), name: NSNotification.Name(GENotification.Galaxy.MergedWithGasPoorDisk), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.displayGasPoorEllipticalCollisionView(_:)), name: NSNotification.Name(GENotification.Galaxy.MergedWithGasPoorElliptical), object: nil)

        // Configure property labels
        [self.totalMassLabel, self.totalMass, self.stellarMassLabel, self.stellarMass, self.gasLevel, self.userType, self.ageLabel, self.ageDilationLabel, self.dilationNumberLabel, self.ageNumberLabel, self.gasAccretionRateLabel].forEach {
            $0?.sizeToFit()
            $0?.textColor = UIColor(white: 0.60, alpha: 1)
        }

        [self.gasLevel, self.totalMass, self.stellarMass, self.userType].forEach {
            $0.text = "-"
        }

        self.ageLabel.text = "Universe Age"
        self.ageNumberLabel.text = "0 myr"
        self.ageDilationLabel.text = "Time Acceleration"
        self.dilationNumberLabel.text = "\(round(10*(Double(Universe.universe.secondEquivalence)/1000000))/10) myr/s"
        self.gasAccretionRateLabel.text = "Gas Accretion Rate"
        self.totalMassLabel.text =   "Total Mass"
        self.stellarMassLabel.text = "Stellar Mass"

        self.pauseButton.setTitle("", for: UIControlState())
        let originalImage = UIImage(named: "glyphicons-175-pause")
        let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        pauseButton.setImage(tintedImage, for: .normal)
        pauseButton.tintColor = .white

        self.pauseButton.sizeToFit()
        self.pauseButton.addTarget(self, action: #selector(GameViewController.pauseGame), for: UIControlEvents.touchUpInside)

        skView = self.view as? SKView
        skView?.showsFPS = false
        skView?.showsNodeCount = false

        //scene.gameSceneDelegate = self
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView?.ignoresSiblingOrder = true
        
        // Set the scene size:
        scene.size = CGSize(width:Universe.universe.userVisibleWidth, height:Universe.universe.userVisibleHeight)
        Qalog("Scene size: \(scene.size)")
        scene.scaleMode = .aspectFill	// the aspect should already match, because it was designed that way in Universe.setDimensionsForScreenDimensions()

        // Pause game so that the Intor view and Decision Tree view can appear
        self.pauseGame()

        // Call functions to set up the Intro view and Decision Tree view
        self.createOverlay()
        self.createIntroView()
        self.createDecisionTreeView()

        // Call functions to show the Intro view
        self.showIntroView()

        skView?.presentScene(scene)

        if !scene.enableZoom {
            self.zoomInButton.isHidden = true
            self.zoomOutButton.isHidden = true
        }

        if Platform.isSimulator {
            self.scene.setValues(-0.05, accelY: -0.05)
        }

        animator = UIDynamicAnimator(referenceView: view)

        //createAlert()

    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func imageWithImage(_ image: UIImage, scaledToSize newSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!;
    }

    func updateAgeProgress(_ notification: Notification) {
        self.universeAgeBar.setProgress(Float(Universe.universe.universeAge / Universe.universe.universeLifespanMillions), animated: true)
        self.ageNumberLabel.text = "\(round(10*Universe.universe.universeAge)/10) myr"
    }

    @IBAction func updateAgeDilation(_ sender: UISlider) {
        Universe.universe.updateSecondEquivalence(Double(sender.value))
        self.dilationNumberLabel.text = "\(round(10*(Double(Universe.universe.secondEquivalence)/1000000))/10) myr/s"
    }

    @IBAction func updateGasAccretion(_ sender: UISlider) {
        Universe.universe.userGalaxy.updateGasAccretionRate(Double(sender.value))
    }

    @IBAction func zoomInAction(_ sender: AnyObject) {
        self.scene.zoomIn()
    }

    @IBAction func zoomOutAction(_ sender: AnyObject) {
        self.scene.zoomOut()
    }

    func pauseGame() {
        Universe.universe.pause()
        self.skView?.isPaused = true
        self.motionMangaer.stopAccelerometerUpdates()
        //self.navigationController?.popViewControllerAnimated(true)
    }

    func resumeGame() {
        self.scene.resetScene()
        Universe.universe.resume()
        self.skView?.isPaused = false
        self.startAccelerometerUpdates()
    }

    func openGalaxyMergersURL() {
        UIApplication.shared.open(URL(string: "http://candels-collaboration.blogspot.com/2012/06/cosmic-collisions-galaxy-mergers-and.html")!,
                                  options: [:],
                                  completionHandler: nil)
    }

    override open func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        self.startAccelerometerUpdates()
    }

    func startAccelerometerUpdates() {
        if self.motionMangaer.isAccelerometerAvailable == true {

            self.motionMangaer.accelerometerUpdateInterval = 0.05

            self.motionMangaer.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: {
                data, error in

                if let data = data {
                    self.scene.setValues(data.acceleration.x, accelY: data.acceleration.y)
                }
            })
        }
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        self.motionMangaer.stopAccelerometerUpdates()
    }

    func updateGalaxy(_ notification: Notification) {
        self.updateGasLevel()
        self.updateUserType()
        self.updateStellarMass()
        self.updateTotalMass()
    }
    
    func updateGasLevel() {
        self.gasLevel.text = "\(Double(round(100*Universe.universe.userGalaxy.gasLevel)/100))% Gas"
    }

    func updateUserType() {
        if let theUserGalaxy = Universe.universe.userGalaxy {
            self.userType.text = "\(theUserGalaxy.category.name())"
        }
    }

    func updateStellarMass() {
        self.stellarMass.text = "\(Int(Universe.universe.userGalaxy.stellarMass))"
    }

    func updateTotalMass() {
        self.totalMass.text = "\(Int(Universe.universe.userGalaxy.totalMass))"
    }
    
    override open var shouldAutorotate : Bool {
        return false
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .portrait
        } else {
            return .all
        }
    }

    override open var prefersStatusBarHidden : Bool {
        return navigationController?.isNavigationBarHidden == true
    }

    override open var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    func createOverlay() {
        // Create a gray view and set its alpha to 0 so it isn't visible
        overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = UIColor(white: 0.05, alpha: 0.95)
        overlayView.alpha = 0.0
        view.addSubview(overlayView)
    }

    // Create the view that will appear at the beginning of the game with which the user can interact to make early evolution decisions
    func createDecisionTreeView() {

        // Here the decision tree view is created
        let overallWidth: CGFloat = self.view.bounds.width
        let overallHeight: CGFloat = self.view.bounds.height
        let overallViewFrame: CGRect = CGRect(x: 0, y: 0, width: overallWidth, height: overallHeight)
        decisionTreeView = UIView(frame: overallViewFrame)
        decisionTreeView.backgroundColor = UIColor.clear
        decisionTreeView.alpha = 0.0
        decisionTreeView.layer.cornerRadius = 0

        // Create a title label
        let titleWidth = overallWidth * 0.75
        let titleHeight = overallHeight * 0.20
        let decisionTreeTitle = UILabel(frame: CGRect(x: ((overallWidth - titleWidth) / 2), y: (overallHeight / 10), width: titleWidth, height: titleHeight))
        decisionTreeTitle.backgroundColor = UIColor.clear
        decisionTreeTitle.text = "Set up your\ngalaxy"
        decisionTreeTitle.textAlignment = .center
        decisionTreeTitle.textColor = UIColor(white: 0.90, alpha: 1.0)
        decisionTreeTitle.font = UIFont(name: "Avenir Next", size: 30)
        decisionTreeTitle.numberOfLines = 0

        // Create density images
        let lowDensityImage = UIImage(named: "Galaxies_Interacting_3")
        let mediumDensityImage = UIImage(named: "Galaxies_Interacting_2")
        let highDensityImage = UIImage(named: "Galaxies_Interacting_3")

        // Set up density image views
        let lowDensityImageView = UIImageView(frame: CGRect(x: (overallWidth / 16), y: (overallHeight * (3/10)), width: (overallWidth / 4), height: (overallWidth / 4)))
        let mediumDensityImageView = UIImageView(frame: CGRect(x: (overallWidth * (6/16)), y: (overallHeight * (3/10)), width: (overallWidth / 4), height: (overallWidth / 4)))
        let highDensityImageView = UIImageView(frame: CGRect(x: (overallWidth * (11/16)), y: (overallHeight * (3/10)), width: (overallWidth / 4), height: (overallWidth / 4)))
        lowDensityImageView.image = lowDensityImage
        mediumDensityImageView.image = mediumDensityImage
        highDensityImageView.image = highDensityImage

        // Create density option buttons
        let lowDensityButton = UIButton(type: UIButtonType.system) as UIButton
        let mediumDensityButton = UIButton(type: UIButtonType.system) as UIButton
        let highDensityButton = UIButton(type: UIButtonType.system) as UIButton

        [lowDensityButton, mediumDensityButton, highDensityButton].forEach {
            $0.setTitleColor(UIColor(white: 0.90, alpha: 1.0), for: UIControlState())
            $0.titleLabel?.font = UIFont(name: "Avenir Next", size: 14)
            $0.titleLabel?.textAlignment = .center
            $0.backgroundColor = UIColor.clear
            $0.isUserInteractionEnabled = false
            $0.titleLabel?.numberOfLines = 0
        }

        lowDensityButton.setTitle("Low\nDensity", for: UIControlState())
        lowDensityButton.frame = CGRect(x: (view.frame.width / 16), y: view.frame.height * (3/10) + lowDensityImageView.frame.height + 5, width: lowDensityImageView.frame.width, height: view.frame.height / 10)
        lowDensityButton.addTarget(self, action: #selector(GameViewController.setUniverseDensityToLow), for: UIControlEvents.touchUpInside)
        lowDensityButton.addTarget(self, action: #selector(GameViewController.dismissDecisionTreeView), for: UIControlEvents.touchUpInside)

        mediumDensityButton.setTitle("Medium\nDensity", for: UIControlState())
        mediumDensityButton.frame = CGRect(x: (view.frame.width * (6/16)), y: view.frame.height * (3/10) + mediumDensityImageView.frame.height + 5, width: lowDensityImageView.frame.width, height: view.frame.height / 10)
        mediumDensityButton.addTarget(self, action: #selector(GameViewController.setUniverseDensityToMedium), for: UIControlEvents.touchUpInside)
        mediumDensityButton.addTarget(self, action: #selector(GameViewController.dismissDecisionTreeView), for: UIControlEvents.touchUpInside)

        highDensityButton.setTitle("High\nDensity", for: UIControlState())
        highDensityButton.frame = CGRect(x: (view.frame.width * (11/16)), y: view.frame.height * (3/10) + highDensityImageView.frame.height + 5, width: lowDensityImageView.frame.width, height: view.frame.height / 10)
        highDensityButton.addTarget(self, action: #selector(GameViewController.setUniverseDensityToHigh), for: UIControlEvents.touchUpInside)
        highDensityButton.addTarget(self, action: #selector(GameViewController.dismissDecisionTreeView), for: UIControlEvents.touchUpInside)


        decisionTreeView.addSubview(decisionTreeTitle)
        decisionTreeView.addSubview(lowDensityImageView)
        decisionTreeView.addSubview(mediumDensityImageView)
        decisionTreeView.addSubview(highDensityImageView)
        decisionTreeView.addSubview(lowDensityButton)
        decisionTreeView.addSubview(mediumDensityButton)
        decisionTreeView.addSubview(highDensityButton)
        view.addSubview(decisionTreeView)
    }

    func showDecisionTreeView() {
        // Enable user interactions with buttons
        decisionTreeView.subviews[4].isUserInteractionEnabled = true
        decisionTreeView.subviews[5].isUserInteractionEnabled = true
        decisionTreeView.subviews[6].isUserInteractionEnabled = true
        
        // Animate in the overlay and Decision Tree view
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 1.0
            self.decisionTreeView.alpha = 1.0
        })
    }

    func dismissDecisionTreeView() {
        // Disable user interactions with buttons
        decisionTreeView.subviews[4].isUserInteractionEnabled = false
        decisionTreeView.subviews[5].isUserInteractionEnabled = false
        decisionTreeView.subviews[6].isUserInteractionEnabled = false

        // Animate out the overlay and Decision Tree view
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            self.decisionTreeView.alpha = 0.0
        })

        resumeGame()
    }

    func setUniverseDensityToLow() {
        Universe.universe.galaxyDensity = 1.0
    }

    func setUniverseDensityToMedium() {
        Universe.universe.galaxyDensity = 2.0
    }

    func setUniverseDensityToHigh() {
        Universe.universe.galaxyDensity = 3.0
    }

    func createIntroView() {

        // Here the decision tree view is created
        let overallWidth: CGFloat = self.view.bounds.width
        let overallHeight: CGFloat = self.view.bounds.height
        let overallViewFrame: CGRect = CGRect(x: 0, y: 0, width: overallWidth, height: overallHeight)
        introView = UIView(frame: overallViewFrame)
        introView.backgroundColor = UIColor.clear
        introView.alpha = 0.0
        introView.layer.cornerRadius = 0

        // Create density images
        let introImage1 = UIImage(named: "Intro_image_1")
        let introImage2 = UIImage(named: "Intro_image_2")
        let introImage3 = UIImage(named: "Intro_image_3")
        let introImage4 = UIImage(named: "Intro_image_4")

        // Set up density image views
        let introImage1View = UIImageView(frame: CGRect(x: 0, y: 0, width: overallWidth, height: (overallHeight * 0.40)))
        let introImage2View = UIImageView(frame: CGRect(x: 0, y: 0, width: overallWidth, height: (overallHeight * 0.40)))
        let introImage3View = UIImageView(frame: CGRect(x: 0, y: 0, width: overallWidth, height: (overallHeight * 0.40)))
        let introImage4View = UIImageView(frame: CGRect(x: 0, y: 0, width: overallWidth, height: (overallHeight * 0.40)))
        introImage1View.image = introImage1
        introImage2View.image = introImage2
        introImage3View.image = introImage3
        introImage4View.image = introImage4

        // Create a title label
        let titleWidth = overallWidth
        let titleHeight = overallHeight * 0.15
        let introTitle1 = UILabel(frame: CGRect(x: ((overallWidth - titleWidth) / 2), y: (overallHeight * 0.42), width: titleWidth, height: titleHeight))
        let introTitle2 = UILabel(frame: CGRect(x: ((overallWidth - titleWidth) / 2), y: (overallHeight * 0.42), width: titleWidth, height: titleHeight))
        let introTitle3 = UILabel(frame: CGRect(x: ((overallWidth - titleWidth) / 2), y: (overallHeight * 0.42), width: titleWidth, height: titleHeight))
        let introTitle4 = UILabel(frame: CGRect(x: ((overallWidth - titleWidth) / 2), y: (overallHeight * 0.42), width: titleWidth, height: titleHeight))

        [introTitle1, introTitle2, introTitle3, introTitle4].forEach {
            $0.backgroundColor = .clear
            $0.textAlignment = .center
            $0.textColor = UIColor(white: 0.90, alpha: 1.0)
            $0.font = UIFont(name: "Avenir Next", size: 28)
            $0.numberOfLines = 0
        }

        introTitle1.text = "BANG!"
        introTitle2.text = "Matter Domination"
        introTitle3.text = "Recombination"
        introTitle4.text = "The First Stars and Galaxies"

        let introText1 = UITextView(frame: CGRect(x: (overallWidth * 0.05), y: (overallHeight * 0.57), width: (overallWidth * 0.90), height: (overallHeight * 0.37)))
        let introText2 = UITextView(frame: CGRect(x: (overallWidth * 0.05), y: (overallHeight * 0.57), width: (overallWidth * 0.90), height: (overallHeight * 0.37)))
        let introText3 = UITextView(frame: CGRect(x: (overallWidth * 0.05), y: (overallHeight * 0.57), width: (overallWidth * 0.90), height: (overallHeight * 0.37)))
        let introText4 = UITextView(frame: CGRect(x: (overallWidth * 0.05), y: (overallHeight * 0.57), width: (overallWidth * 0.90), height: (overallHeight * 0.37)))

        [introText1, introText2, introText3, introText4].forEach {
            $0.backgroundColor = .clear
            $0.textAlignment = .left
            $0.textColor = UIColor(white: 0.90, alpha: 1.0)
            $0.font = UIFont(name: "Avenir Next", size: 16)
            $0.isUserInteractionEnabled = false
        }

        introText1.text = "The universe begins in an explosion 13.7 billion years ago. Light particles, called photons, were created in about 10 seconds after the Big Bang."
        introText2.text = "The first particles began appear about 70,000 years after the Big Bang. Quarks, electrons, and protons are made during this period."
        introText3.text = "Gases like hydrogen and helium are slowly forming. Matter clumps together by gravitational attraction and eventually forms stars and galaxies. We see this by looking at         microwaves in space – called the Cosmic Microwave Background Radiation. "
        introText4.text = "It’s about one hundred million years after the Big Bang. We’ve arrived in the epoch where stars and galaxies are forming.  Now you get to form your own galaxy!"

        introImage2View.alpha = 0.0
        introImage3View.alpha = 0.0
        introImage4View.alpha = 0.0
        introTitle2.alpha = 0.0
        introTitle3.alpha = 0.0
        introTitle4.alpha = 0.0
        introText2.alpha = 0.0
        introText3.alpha = 0.0
        introText4.alpha = 0.0

        introView.addSubview(introImage1View)
        introView.addSubview(introTitle1)
        introView.addSubview(introText1)

        introView.addSubview(introImage2View)
        introView.addSubview(introTitle2)
        introView.addSubview(introText2)

        introView.addSubview(introImage3View)
        introView.addSubview(introTitle3)
        introView.addSubview(introText3)

        introView.addSubview(introImage4View)
        introView.addSubview(introTitle4)
        introView.addSubview(introText4)

        view.addSubview(introView)

    }

    func showIntroView() {
        // Animate in the overlay and Intro View
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 1.0
            self.introView.alpha = 1.0
        })

        self.createTapGestureRecognizer()
    }

    func createTapGestureRecognizer() {
        let dismissIntroTapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GameViewController.handleIntroDismissTap(_:)))
        view.addGestureRecognizer(dismissIntroTapGestureRecognizer)
    }

    func handleIntroDismissTap(_ sender: UITapGestureRecognizer) {
        self.introCounter += 1

        if introCounter == 2 {
            UIView.animate(withDuration: 0.4, animations: {
                self.introView.subviews[0].alpha = 0.0
                self.introView.subviews[1].alpha = 0.0
                self.introView.subviews[2].alpha = 0.0

                self.introView.subviews[3].alpha = 1.0
                self.introView.subviews[4].alpha = 1.0
                self.introView.subviews[5].alpha = 1.0
            })
        }

        if introCounter == 3 {
            UIView.animate(withDuration: 0.4, animations: {
                self.introView.subviews[3].alpha = 0.0
                self.introView.subviews[4].alpha = 0.0
                self.introView.subviews[5].alpha = 0.0

                self.introView.subviews[6].alpha = 1.0
                self.introView.subviews[7].alpha = 1.0
                self.introView.subviews[8].alpha = 1.0
            })
        }

        if introCounter == 4 {
            UIView.animate(withDuration: 0.4, animations: {
                self.introView.subviews[6].alpha = 0.0
                self.introView.subviews[7].alpha = 0.0
                self.introView.subviews[8].alpha = 0.0

                self.introView.subviews[9].alpha = 1.0
                self.introView.subviews[10].alpha = 1.0
                self.introView.subviews[11].alpha = 1.0
            })
        }

        if introCounter > 4 {
            UIView.animate(withDuration: 0.4, animations: {
                self.introView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.introView.removeFromSuperview()
                self.introView = nil
            })

            self.view.removeGestureRecognizer(sender)
            self.showDecisionTreeView()
        }
    }


    func createCollisionAlert(_ image: UIImage, text: String) {
        // Here the alert view is created. It is created with rounded corners and given a shadow around it
        let alertWidth: CGFloat = 250
        let alertHeight: CGFloat = 410
        let alertViewFrame: CGRect = CGRect(x: 0, y: 0, width: alertWidth, height: alertHeight)
        collisionAlertView = UIView(frame: alertViewFrame)
        collisionAlertView.backgroundColor = UIColor.white
        collisionAlertView.alpha = 0.0
        collisionAlertView.layer.cornerRadius = 5
        collisionAlertView.layer.shadowColor = UIColor.black.cgColor
        collisionAlertView.layer.shadowOffset = CGSize(width: 0, height: 5)
        collisionAlertView.layer.shadowOpacity = 0.3
        collisionAlertView.layer.shadowRadius = 10.0

        // Create a button and set a listener on it for when it is tapped. Then the button is added to the alert view
        let resumeButton = UIButton(type: UIButtonType.system) as UIButton
        resumeButton.setTitle("Resume", for: UIControlState())
        resumeButton.setTitleColor(UIColor.blue, for: UIControlState())
        resumeButton.titleLabel?.font = UIFont(name: "System", size: 16)
        resumeButton.backgroundColor = UIColor.clear
        resumeButton.layer.cornerRadius = 5
        resumeButton.frame = CGRect(x: 0, y: alertHeight - 40.0, width: alertWidth/2, height: 40.0)
        resumeButton.addTarget(self, action: #selector(GameViewController.resumeGame), for: UIControlEvents.touchUpInside)
        resumeButton.addTarget(self, action: #selector(GameViewController.dismissCollisionAlert(_:)), for: UIControlEvents.touchUpInside)

        let readMoreButton = UIButton(type: UIButtonType.system) as UIButton
        readMoreButton.setTitle("Read More", for: UIControlState())
        readMoreButton.setTitleColor(UIColor.blue, for: UIControlState())
        readMoreButton.titleLabel?.font = UIFont(name: "System", size: 16)
        readMoreButton.backgroundColor = UIColor.clear
        readMoreButton.layer.cornerRadius = 5
        readMoreButton.frame = CGRect(x: alertWidth/2, y: alertHeight - 40.0, width: alertWidth/2, height: 40.0)
        //readMoreButton.addTarget(self, action: #selector(GameViewController.dismissCollisionAlert(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        readMoreButton.addTarget(self, action: #selector(GameViewController.openGalaxyMergersURL), for: UIControlEvents.touchUpInside)

        let imageViewHeight : CGFloat = 150.0
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: alertWidth, height: imageViewHeight))
        imageView.image = image

        /*
        let itemSize = CGSizeMake(alertWidth, 150)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.mainScreen().scale)
        let imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height)
        imageView.image!.drawInRect(imageRect)
        imageView.image! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        */

        let textView = UITextView(frame: CGRect(x: 0, y: imageViewHeight, width: alertWidth, height: alertHeight - imageViewHeight - resumeButton.frame.height))
        textView.text = text

        collisionAlertView.addSubview(resumeButton)
        collisionAlertView.addSubview(readMoreButton)
        collisionAlertView.addSubview(imageView)
        collisionAlertView.addSubview(textView)
        view.addSubview(collisionAlertView)
    }

    func createAlert() {
        let alertWidth: CGFloat = 150
        let alertHeight: CGFloat = 80
        let alertViewFrame: CGRect = CGRect(x: 0, y: 0, width: alertWidth, height: alertHeight)
        alertView = UIView(frame: alertViewFrame)
        alertView.backgroundColor = UIColor.white
        alertView.alpha = 0.0
        alertView.layer.cornerRadius = 5
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOffset = CGSize(width: 0, height: 5)
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowRadius = 10.0

        // Create a button and set a listener on it for when it is tapped. Then the button is added to the alert view
        let resumeButton = UIButton(type: UIButtonType.system) as UIButton
        resumeButton.setTitle("Resume", for: UIControlState())
        resumeButton.setTitleColor(UIColor(white: 0.05, alpha: 1), for: UIControlState())
        resumeButton.titleLabel?.font = UIFont(name: "System", size: 16)
        resumeButton.backgroundColor = UIColor.clear
        resumeButton.frame = CGRect(x: 0, y: 15, width: alertWidth, height: 20.0)
        resumeButton.addTarget(self, action: #selector(GameViewController.resumeGame), for: UIControlEvents.touchUpInside)
        resumeButton.addTarget(self, action: #selector(GameViewController.dismissAlert(_:)), for: UIControlEvents.touchUpInside)

        let menuButton = UIButton(type: UIButtonType.system) as UIButton
        menuButton.setTitle("Menu", for: UIControlState())
        menuButton.setTitleColor(UIColor(white: 0.05, alpha: 1), for: UIControlState())
        menuButton.titleLabel?.font = UIFont(name: "System", size: 16)
        menuButton.backgroundColor = UIColor.clear
        menuButton.frame = CGRect(x: 0, y: 45, width: alertWidth, height: 20.0)
        menuButton.addTarget(self, action: #selector(GameViewController.dismissAlert(_:)), for: UIControlEvents.touchUpInside)
        menuButton.addTarget(self, action: #selector(GameViewController.returnToMenu), for: UIControlEvents.touchUpInside)

        alertView.addSubview(resumeButton)
        alertView.addSubview(menuButton)
        view.addSubview(alertView)
    }

    func returnToMenu() {
        performSegue(withIdentifier: "returnToMenuFromGame", sender: self)
        self.ageDilationSlider.value = -2550
        Universe.universe.reset()
    }

    func showAlert() {
        // When the alert view is dismissed, I destroy it, so I check for this condition here
        // since if the Show Alert button is tapped again after dismissing, alertView will be nil
        // and so should be created again
        if (alertView == nil) {
            createAlert()
        }

        // I create the pan gesture recognizer here and not in ViewDidLoad() to
        // prevent the user moving the alert view on the screen before it is shown.
        // Remember, on load, the alert view is created but invisible to user, so you
        // don't want the user moving it around when they swipe or drag on the screen.

        animator.removeAllBehaviors()

        // Animate in the overlay
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.8
            self.alertView.alpha = 0.8
        }) 

        // Animate the alert view using UIKit Dynamics.

        let snapBehaviour: UISnapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
        animator.addBehavior(snapBehaviour)
    }

    func showCollisionAlert(_ image: UIImage, text: String) {
        // When the alert view is dismissed, I destroy it, so I check for this condition here
        // since if the Show Alert button is tapped again after dismissing, alertView will be nil
        // and so should be created again
        if (collisionAlertView == nil) {
            createCollisionAlert(image, text: text)
        }

        // I create the pan gesture recognizer here and not in ViewDidLoad() to
        // prevent the user moving the alert view on the screen before it is shown.
        // Remember, on load, the alert view is created but invisible to user, so you
        // don't want the user moving it around when they swipe or drag on the screen.

        animator.removeAllBehaviors()

        // Animate in the overlay
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.8
            self.collisionAlertView.alpha = 0.8
        }) 

        // Animate the alert view using UIKit Dynamics.
        //alertView.alpha = 0.8

        let snapBehaviour: UISnapBehavior = UISnapBehavior(item: collisionAlertView, snapTo: view.center)
        animator.addBehavior(snapBehaviour)
    }

    func dismissAlert(_ sender:AnyObject?) {
        animator.removeAllBehaviors()

        let gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [alertView])
        gravityBehaviour.gravityDirection = CGVector(dx: 0.0, dy: 10.0);
        animator.addBehavior(gravityBehaviour)

        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        let itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [alertView])
        itemBehaviour.addAngularVelocity(CGFloat(-(Double.pi/2)), for: alertView)
        animator.addBehavior(itemBehaviour)

        // Animate out the overlay, remove the alert view from its superview and set it to nil
        // If you don't set it to nil, it keeps falling off the screen and when Show Alert button is
        // tapped again, it will snap into view from below. It won't have the location settings we defined in createAlert()
        // And the more it 'falls' off the screen, the longer it takes to come back into view, so when the Show Alert button
        // is tapped again after a considerable time passes, the app seems unresponsive for a bit of time as the alert view
        // comes back up to the screen
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.alertView.removeFromSuperview()
                self.alertView = nil
        })

        //self.resumeGame()
    }

    func dismissCollisionAlert(_ sender:AnyObject?) {
        animator.removeAllBehaviors()

        let gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [collisionAlertView])
        gravityBehaviour.gravityDirection = CGVector(dx: 0.0, dy: 10.0);
        animator.addBehavior(gravityBehaviour)

        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        let itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [collisionAlertView])
        itemBehaviour.addAngularVelocity(CGFloat(-(Double.pi/2)), for: collisionAlertView)
        animator.addBehavior(itemBehaviour)

        // Animate out the overlay, remove the alert view from its superview and set it to nil
        // If you don't set it to nil, it keeps falling off the screen and when Show Alert button is
        // tapped again, it will snap into view from below. It won't have the location settings we defined in createAlert()
        // And the more it 'falls' off the screen, the longer it takes to come back into view, so when the Show Alert button
        // is tapped again after a considerable time passes, the app seems unresponsive for a bit of time as the alert view
        // comes back up to the screen
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.collisionAlertView.removeFromSuperview()
                self.collisionAlertView = nil
        })

        //self.resumeGame()
    }

    @IBAction func showAlertView(_ sender: UIButton) {
        showAlert()
    }

    func displayGasRichDiskCollisionView(_ notification: Notification) {
        self.pauseGame()
        let image = UIImage(named: "Galaxies_Interacting")!
        let text = "You have merged with a Gas Rich Disk Galaxy! Merging with other galaxies is a significant part of this game and greatly affects the properties of your galaxy. The effects of a merger depend on the properties of the galaxy you merge with as well as your galaxy's properties. During a merger, the other galaxy's total mass is added to your galaxy's total mass. Additionally, a merger sometimes spurs rapid star formation in an event called a starburst (which you can learn more about if you tap \"Read More\"). This event causes some of your gas mass to be converted into stellar mass."
        self.createCollisionAlert(image , text: text)
        self.showCollisionAlert(image, text: text)
    }

    func displayGasPoorDiskCollisionView(_ notification: Notification) {
        self.pauseGame()
        let image = UIImage(named: "Galaxies_Interacting_3")!
        let text = "Test text gas poor disk. The text here will explain what this merger is and its results."
        self.createCollisionAlert(image , text: text)
        self.showCollisionAlert(image, text: text)
    }

    func displayGasPoorEllipticalCollisionView(_ notification: Notification) {
        self.pauseGame()
        let image = UIImage(named: "Galaxies_Interacting_2")!
        let text = "Test text gas poor elliptical. The text here will explain what this merger is and its results."
        self.createCollisionAlert(image , text: text)
        self.showCollisionAlert(image, text: text)
    }

    /*func createGestureRecognizer() {
        let panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(PhotoViewController.handlePan(_:)))
        view.addGestureRecognizer(panGestureRecognizer)
    }

    func handlePan(_ sender: UIPanGestureRecognizer) {

        if (alertView != nil) {
            //let panLocationInView = sender.locationInView(view)
            //let panLocationInAlertView = sender.locationInView(alertView)

            /*if sender.state == UIGestureRecognizerState.Began {
                animator.removeAllBehaviors()

                let offset = UIOffsetMake(panLocationInAlertView.x - CGRectGetMidX(alertView.bounds), panLocationInAlertView.y - CGRectGetMidY(alertView.bounds));
                attachmentBehavior = UIAttachmentBehavior(item: alertView, offsetFromCenter: offset, attachedToAnchor: panLocationInView)

                animator.addBehavior(attachmentBehavior)
            }
            if sender.state == UIGestureRecognizerState.Changed {
                attachmentBehavior.anchorPoint = panLocationInView
            }*/
            if sender.state == UIGestureRecognizerState.ended {
                animator.removeAllBehaviors()

                snapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
                animator.addBehavior(snapBehavior)

                if sender.translation(in: view).y > 100 {
                    dismissAlert(sender)
                    resumeGame()
                }
            }
        }

    }*/

}
