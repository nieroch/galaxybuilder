//
//  GlossaryManager.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/26/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Manager class to set up lists and arrays for the Glossary
open class GlossaryManager {
    fileprivate var items : [GlossaryItem] = []

    fileprivate var links = [("Hubble Galaxy Images", URL(string: "http://hubblesite.org/gallery/album/galaxy/")),
                             ("Hubble Site", URL(string: "http://hubblesite.org")),
                             ("Galaxy Mergers", URL(string: "http://candels-collaboration.blogspot.com/2012/06/cosmic-collisions-galaxy-mergers-and.html")),
                             ("Types of Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/06/what-types-of-galaxies-are-there.html")),
                             ("Galaxy Morphology", URL(string: "http://candels-collaboration.blogspot.com/2012/08/how-do-we-measure-galaxy-morphology.html")),
                             ("Galaxy Zoo Project", URL(string: "http://candels-collaboration.blogspot.com/2012/10/galaxy-zoo-meets-candels.html")),
                             ("Black Holes", URL(string: "http://candels-collaboration.blogspot.de/2012/06/supermassive-black-holes-and-active.html")),
                             ("Dead Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/08/what-turns-galaxies-off.html")),
                             ("Active Galactic Nuclei", URL(string: "https://www.nasa.gov/subject/8732/active-galactic-nuclei/")),
                             ("Star Formation Rate", URL(string: "https://astrobites.org/2014/09/30/measuring-galaxy-star-formation/")),
                             ("Stellar Mass", URL(string: "https://science.nasa.gov/astrophysics/focus-areas/how-do-stars-form-and-evolve")),
                             ("Big Bang", URL(string: "https://home.cern/about/physics/early-universe")),
                             ("CMB", URL(string: "https://map.gsfc.nasa.gov/universe/bb_tests_cmb.html")),
                             ]

    fileprivate var descriptionTitles : [String] = ["Active Galactic Nuclei (AGN)",
                                                    "Star Formation Rate (SFR)",
                                                    "Stellar Mass",
                                                    "Morphology",
                                                    "Spiral Galaxy",
                                                    "Elliptical Galaxy",
                                                    "Lenticular Galaxy",
                                                    "Item 8 Title",
                                                    "Item 9 Title",
                                                    "Item 10 Title",
                                                    "Item 11 Title",
                                                    "Item 12 Title",
                                                    "Item 13 Title",
                                                    "Item 14 Title",
                                                    "Big Bang",
                                                    "Item 16 Title",
                                                    "Cosmic Microwave\nBackground Radiation",
                                                    "Item 18 Title",
                                                    "Item 19 Title",
                                                    "Item 20 Title",
                                                    "Item 21 Title",
                                                    "Item 22 Title",
                                                    "Item 23 Title",
                                                    "Item 24 Title",
                                                    "Item 25 Title",
                                                    "Item 26 Title"
                                                    ]

    fileprivate var descriptions : [String] = ["A supermassive black hole at the center of a galaxy that is accreting matter. When dust and gas falls into the black hole, it releases an enormous amount of energy that we can see with space telescopes.",
                                               "How fast stars form within a galaxy. The greater the SFR number, the faster the galaxy is forming stars.",
                                               "How massive stars and galaxies are compared to the Sun’s Mass (1.99 * 10^30 kg/ 4.39 * 10^30 lbs).",
                                               "The shape of a galaxy.",
                                               "A type of galaxy that is gas-rich, disk-shaped, and is actively forming stars.",
                                               "A type of galaxy that is gas-poor, looks like a sphere or oval, and is not forming many stars.",
                                               "A type of galaxy that is shaped similarly to an elliptical galaxy, but still has a gaseous disk.",
                                               "Item 8 description",
                                               "Item 9 description",
                                               "Item 10 description",
                                               "Item 11 description",
                                               "Cluster of galaxies",
                                               "Many galaxies",
                                               "Galaxies interacting",
                                               "A theory that describes what astronomers believe to be the beginning of the Universe. We can see the Universe expanding by looking at galaxies far away, as well as the Cosmic Microwave Background Radiation.",
                                               "Item 16 description",
                                               "The light we see that marks the Recombination Era of the early universe. Recombination is when the first stars produced light energy to ionize particles so those particles made their own light. For more information about the CMB, click here.",
                                               "Item 18 description",
                                               "Item 19 description",
                                               "Item 20 description",
                                               "Item 21 description",
                                               "Item 22 description",
                                               "Item 23 description",
                                               "Item 24 description",
                                               "Item 25 description",
                                               "Item 26 description",
                                               ]

    fileprivate var types : [String] = ["Glossary",
                                         "Glossary",
                                         "Glossary",
                                         "Glossary",
                                         "Glossary",
                                         "Glossary",
                                         "Glossary",
                                         "Links",
                                         "Links",
                                         "Links",
                                         "Links",
                                         "Links",
                                         "Links",
                                         "Links",
                                         "Glossary",
                                         "Links",
                                         "Glossary",
                                         "Links",
                                         "Glossary",
                                         "Links",
                                         "Glossary",
                                         "Links",
                                         "Glossary",
                                         "Links",
                                         "Glossary",
                                         "Links"
                                         ]



    static let sharedInstance = GlossaryManager()

    fileprivate init() {
        self.items.append(GlossaryItem(name: "Active Galactic Nuclei (AGN)", description: descriptions[0], descriptionTitle: descriptionTitles[0], type: types[0]))
        self.items.append(GlossaryItem(name: "Star Formation Rate (SFR)", description: descriptions[1], descriptionTitle: descriptionTitles[1], type: types[1]))
        self.items.append(GlossaryItem(name: "Stellar Mass", description: descriptions[2], descriptionTitle: descriptionTitles[2], type: types[2]))
        self.items.append(GlossaryItem(name: "Morphology", description: descriptions[3], descriptionTitle: descriptionTitles[3], type: types[3]))
        self.items.append(GlossaryItem(name: "Spiral Galaxy", description: descriptions[4], descriptionTitle: descriptionTitles[4], type: types[4]))
        self.items.append(GlossaryItem(name: "Elliptical Galaxy", description: descriptions[5], descriptionTitle: descriptionTitles[5], type: types[5]))
        self.items.append(GlossaryItem(name: "Lenticular Galaxy", description: descriptions[6], descriptionTitle: descriptionTitles[6], type: types[6]))
        self.items.append(GlossaryItem(name: "Hubble Galaxy Images", description: descriptions[7], descriptionTitle: descriptionTitles[7], type: types[7]))
        self.items.append(GlossaryItem(name: "Hubble Site", description: descriptions[8], descriptionTitle: descriptionTitles[8], type: types[8]))
        self.items.append(GlossaryItem(name: "Galaxy Mergers", description: descriptions[9], descriptionTitle: descriptionTitles[9], type: types[9]))
        self.items.append(GlossaryItem(name: "Types of Galaxies", description: descriptions[10], descriptionTitle: descriptionTitles[10], type: types[10]))
        self.items.append(GlossaryItem(name: "Galaxy Morphology", description: descriptions[11], descriptionTitle: descriptionTitles[11], type: types[11]))
        self.items.append(GlossaryItem(name: "Galaxy Zoo Project", description: descriptions[12], descriptionTitle: descriptionTitles[12], type: types[12]))
        self.items.append(GlossaryItem(name: "Black Holes", description: descriptions[13], descriptionTitle: descriptionTitles[13], type: types[13]))
        self.items.append(GlossaryItem(name: "Big Bang", description: descriptions[14], descriptionTitle: descriptionTitles[14], type: types[14]))
        self.items.append(GlossaryItem(name: "Dead Galaxies", description: descriptions[15], descriptionTitle: descriptionTitles[15], type: types[15]))
        self.items.append(GlossaryItem(name: "Cosmic Microwave Background Radiation (CMB)", description: descriptions[16], descriptionTitle: descriptionTitles[16], type: types[16]))
        self.items.append(GlossaryItem(name: "Active Galactic Nuclei", description: descriptions[17], descriptionTitle: descriptionTitles[17], type: types[17]))
        self.items.append(GlossaryItem(name: "Item 19", description: descriptions[18], descriptionTitle: descriptionTitles[18], type: types[18]))
        self.items.append(GlossaryItem(name: "Star Formation Rate", description: descriptions[19], descriptionTitle: descriptionTitles[19], type: types[19]))
        self.items.append(GlossaryItem(name: "Item 21", description: descriptions[20], descriptionTitle: descriptionTitles[20], type: types[20]))
        self.items.append(GlossaryItem(name: "Stellar Mass", description: descriptions[21], descriptionTitle: descriptionTitles[21], type: types[21]))
        self.items.append(GlossaryItem(name: "Item 23", description: descriptions[22], descriptionTitle: descriptionTitles[22], type: types[22]))
        self.items.append(GlossaryItem(name: "Big Bang", description: descriptions[23], descriptionTitle: descriptionTitles[23], type: types[23]))
        self.items.append(GlossaryItem(name: "Item 25", description: descriptions[24], descriptionTitle: descriptionTitles[24], type: types[24]))
        self.items.append(GlossaryItem(name: "CMB", description: descriptions[25], descriptionTitle: descriptionTitles[25], type: types[25]))

    }

    open func glossaryItemsList() -> [GlossaryItem] {
        return items
    }

    open func linkAtIndex(_ index: Int) -> (title: String, url: URL?) {
        return links[index]
    }

}









