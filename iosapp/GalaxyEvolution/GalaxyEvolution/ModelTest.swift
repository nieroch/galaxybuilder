//
//  ModelTest.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/24/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation

// Used to test out aspects of the model without running a full simulation of the app

let galaxy1 = Galaxy(mass: 500, blackHoleMass: 0, stellarMass: 0, shape: Galaxy.ShapeCategory.spiral)           //user galaxy
let galaxy2 = Galaxy(mass: 100000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich massive
let galaxy3 = Galaxy(mass: 20000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich normal
let galaxy4 = Galaxy(mass: 3000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich light
let galaxy5 = Galaxy(mass: 400, blackHoleMass: 90, stellarMass: 90, shape: Galaxy.ShapeCategory.spiral)  //gas rich very light
let galaxy6 = Galaxy(mass: 200000, blackHoleMass: 60000, stellarMass: 60000, shape: Galaxy.ShapeCategory.elliptical)  //gas poor massive
let galaxy7 = Galaxy(mass: 30000, blackHoleMass: 9000, stellarMass: 9000, shape: Galaxy.ShapeCategory.elliptical)  //gas poor normal
let galaxy8 = Galaxy(mass: 4000, blackHoleMass: 1500, stellarMass: 1500, shape: Galaxy.ShapeCategory.elliptical)  //gas poor light
let galaxy9 = Galaxy(mass: 500, blackHoleMass: 200, stellarMass: 200, shape: Galaxy.ShapeCategory.elliptical)  //gas poor very light

func test1() {
    Qalog()

    //let userGalaxy = Universe.universe.userGalaxy
    //let galaxies = Universe.universe.galaxies
    let galaxyTest2 = Galaxy(mass: 100000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich massive
    let galaxyTest3 = Galaxy(mass: 20000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich normal
    let galaxyTest4 = Galaxy(mass: 3000, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.spiral)  //gas rich light
    let galaxyTest5 = Galaxy(mass: 400, blackHoleMass: 90, stellarMass: 90, shape: Galaxy.ShapeCategory.spiral)  //gas rich very light
    let galaxyTest6 = Galaxy(mass: 200000, blackHoleMass: 60000, stellarMass: 60000, shape: Galaxy.ShapeCategory.elliptical)  //gas poor massive

    Universe.universe.addGalaxy(galaxyTest2)
    Universe.universe.addGalaxy(galaxyTest3)
    Universe.universe.addGalaxy(galaxyTest4)
    Universe.universe.addGalaxy(galaxyTest5)
    Universe.universe.addGalaxy(galaxyTest6)

    //Universe.universe.printUniverse()

    //printStatus([0, 1, 2, 17])

    print("Number of galaxies: \(Universe.universe.galaxies.count)")
    galaxyTest2.mergeWithGalaxy(galaxyTest3)
    print("Number of galaxies: \(Universe.universe.galaxies.count)")

    //print("After merges:")
    //printStatus([0, 1, 2, 16, 17])

}

func testMerge2() {
    //Qalog()

    /*
    let galaxy1 = Galaxy(mass: 500, sfr: 500, blackHoleMass: 0, stellarMass: 0, shape: Galaxy.ShapeCategory.Spiral)           //user galaxy
    let galaxy2 = Galaxy(mass: 100000, sfr: 400, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.Spiral)  //gas rich massive
    let galaxy3 = Galaxy(mass: 20000, sfr: 300, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.Spiral)  //gas rich normal
    let galaxy4 = Galaxy(mass: 3000, sfr: 200, blackHoleMass: 500, stellarMass: 500, shape: Galaxy.ShapeCategory.Spiral)  //gas rich light
    let galaxy5 = Galaxy(mass: 400, sfr: 100, blackHoleMass: 90, stellarMass: 90, shape: Galaxy.ShapeCategory.Spiral)  //gas rich very light
    let galaxy6 = Galaxy(mass: 200000, sfr: 400, blackHoleMass: 60000, stellarMass: 60000, shape: Galaxy.ShapeCategory.Elliptical)  //gas poor massive
    let galaxy7 = Galaxy(mass: 30000, sfr: 300, blackHoleMass: 9000, stellarMass: 9000, shape: Galaxy.ShapeCategory.Elliptical)  //gas poor normal
    let galaxy8 = Galaxy(mass: 4000, sfr: 200, blackHoleMass: 1500, stellarMass: 1500, shape: Galaxy.ShapeCategory.Elliptical)  //gas poor light
    let galaxy9 = Galaxy(mass: 500, sfr: 100, blackHoleMass: 200, stellarMass: 200, shape: Galaxy.ShapeCategory.Elliptical)  //gas poor very light
    */

    func status(_ state:String) {
        print("\(state)")
        galaxy1.printGalaxy("Galaxy 1")
        galaxy2.printGalaxy("Galaxy 2")
    }

    //status("Before")
    //galaxy1.mergeWithGalaxy(galaxy2)
    //status("After")

    galaxy1.mergeWithGalaxy(galaxy2)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy3)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy4)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy5)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy6)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy7)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy8)
    galaxy1.printGalaxy("User Galaxy")

    galaxy1.mergeWithGalaxy(galaxy9)
    galaxy1.printGalaxy("User Galaxy")

}

func test3() {
    galaxy1.printGalaxy("User Galaxy")
    galaxy1.mergeWithGalaxy(galaxy9)
    galaxy1.printGalaxy("User Galaxy")
}

func testCreateSpaceObjects() {
    var objectCount : [Galaxy.GalaxyType : Int] = [:]
    for i in 0 ..< 1000 {
        let newObject = Universe.universe.typeForNewSpaceObject(0.0)
        if i < 30 {
            print(newObject.name())
        }
        if objectCount[newObject] == nil {
            objectCount[newObject] = 0
        }
        objectCount[newObject]! += 1
    }
    // list counts:
    for objectType in Galaxy.GalaxyType.allTypes() {
        print("\(objectType.name()) : \(objectCount[objectType])")
    }
}

private func printStatus(_ galaxyIndices:[Int]) {
    Universe.universe.userGalaxy.printGalaxy("User Galaxy")
    for i in galaxyIndices {
        Universe.universe.galaxies[i].printGalaxy("Galaxy \(i)")
    }
}













