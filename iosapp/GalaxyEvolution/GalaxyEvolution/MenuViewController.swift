//
//  MenuViewController.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/8/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import UIKit

// Class that creates the animated star background
class StarCurtain: UIView {

    static let useForScreenShot = false

    init(width: CGFloat, height: CGFloat, starDensity: Float, opacity: Float, color: UIColor) {

        var starSize : CGFloat
        if StarCurtain.useForScreenShot {
            starSize = 1.0
        } else {
            starSize = 0.5
        }

        let extendedHeight = 2 * height
        
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: extendedHeight))
        
        self.layer.opacity = opacity
        
        let starCount = Int(starDensity * Float(width * height))
        
        for _ in 0 ..< starCount {
            let xPos = CGFloat(drand48()) * width
            let yPos = CGFloat(drand48()) * height
            
            let star = UIView(frame: CGRect(x: xPos, y: yPos, width: starSize, height: starSize))
            star.backgroundColor = color
            self.addSubview(star)
            
            let mirrorStar = UIView(frame: CGRect(x: xPos, y: yPos + height, width: starSize, height: starSize))
            mirrorStar.backgroundColor = color
            self.addSubview(mirrorStar)
        }
        
        resetPosition()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func resetPosition() {
        self.frame.origin.y = -frame.height/2
    }
    
}

// View controller for the menu
class MenuViewController: UIViewController {

    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    //@IBOutlet weak var learnMoreButton: UIButton!
    @IBOutlet weak var ritButton: UIButton!
    @IBOutlet weak var achievementsButton: UIButton!
    @IBOutlet weak var glossaryButton: UIButton!
    @IBOutlet var newHeightContraint: NSLayoutConstraint!
    @IBOutlet var galleryHeightContraint: NSLayoutConstraint!
    //@IBOutlet var learnHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var ritHeightConstraint: NSLayoutConstraint!
    @IBOutlet var newWidthContraint: NSLayoutConstraint!
    @IBOutlet var galleryWidthContraint: NSLayoutConstraint!
    //@IBOutlet var learnWidthContraint: NSLayoutConstraint!
    @IBOutlet weak var ritWidthConstraint: NSLayoutConstraint!
    @IBOutlet var menuView: UIView!
    @IBOutlet var menuBar: UINavigationItem!
    @IBOutlet weak var titleLabel: UILabel!

    
    var starCurtains : [StarCurtain] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "candels_background.jpg")!)
        self.view.backgroundColor = UIColor.init(white: 0.0, alpha: 1)
        self.navigationController?.isNavigationBarHidden = true
        
        starCurtains.append(StarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.35, color: UIColor.white))
        starCurtains.append(StarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.50, color: UIColor.white))
        starCurtains.append(StarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.65, color: UIColor.white))
        starCurtains.append(StarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.80, color: UIColor.white))

        for curtain in starCurtains {
            self.view.insertSubview(curtain, at: 0)
        }

        titleLabel.text = "Galaxy\nBuilder"
        titleLabel.textColor = UIColor.init(white: 1.0, alpha: 1)

        newGameButton.setTitle("", for: UIControlState())
        var originalImage = UIImage(named: "glyphicons-174-play-big")
        var tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        newGameButton.setImage(tintedImage, for: .normal)
        newGameButton.tintColor = .white
        newGameButton.imageView?.contentMode = .scaleAspectFit

        galleryButton.setTitle("", for: UIControlState())
        originalImage = UIImage(named: "glyphicons-320-sort")
        tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        galleryButton.setImage(tintedImage, for: .normal)
        galleryButton.tintColor = .white

        //learnMoreButton.setTitle("Learn More", for: UIControlState())
        ritButton.setTitle("", for: UIControlState())
        originalImage = UIImage(named: "glyphicons-196-info-sign")
        tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        ritButton.setImage(tintedImage, for: .normal)
        ritButton.tintColor = .white

        achievementsButton.setTitle("", for: UIControlState())
        originalImage = UIImage(named: "glyphicons-75-cup")
        tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        achievementsButton.setImage(tintedImage, for: .normal)
        achievementsButton.tintColor = .white


        glossaryButton.setTitle("", for: UIControlState())
        originalImage = UIImage(named: "glyphicons-40-notes")
        tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        glossaryButton.setImage(tintedImage, for: .normal)
        glossaryButton.tintColor = .white


        //newGameButton.setTitleColor(UIColor.init(white: 0.80, alpha: 1), forState: UIControlState.Normal)
        //galleryButton.setTitleColor(UIColor.init(white: 0.80, alpha: 1), forState: UIControlState.Normal)
        //learnMoreButton.setTitleColor(UIColor.init(white: 0.80, alpha: 1), forState: UIControlState.Normal)


        [newGameButton, galleryButton, ritButton, achievementsButton, glossaryButton].forEach {
            $0?.setTitleColor(UIColor(white: 0.90, alpha: 1), for: UIControlState())
            $0?.layer.cornerRadius = 5.0
            $0?.layer.borderWidth = 1.5
            $0?.layer.borderColor = UIColor.clear.cgColor
            $0?.backgroundColor = UIColor(white: 0.80, alpha: 0.0)
        }

        self.newGameButton.addTarget(Universe.universe, action: #selector(Universe.startEvolution), for: UIControlEvents.touchUpInside)

        /*let newFrm = newGameButton.frame
        let galleryFrm = galleryButton.frame
        let learnFrm = learnMoreButton.frame
        
        newGameButton.frame = CGRectMake(newFrm.origin.x - 20, newFrm.origin.y - 5, newFrm.width + 40, newFrm.height + 10)
        galleryButton.frame = CGRectMake(galleryFrm.origin.x - 20, galleryFrm.origin.y - 5, galleryFrm.width + 40, galleryFrm.height + 10)
        learnMoreButton.frame = CGRectMake(learnFrm.origin.x - 20, learnFrm.origin.y - 5, learnFrm.width + 40, learnFrm.height + 10)
        */

        // Do any additional setup after loading the view.
    }
    
    func animateStars() {
        
        func animateCurtain(_ curtain: StarCurtain, duration: TimeInterval) {
            
            curtain.resetPosition()
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [UIViewAnimationOptions.curveLinear, UIViewAnimationOptions.repeat], animations: {
                
                curtain.frame.origin.y = 0.0
                
            }) { (Bool) in
                //completion?()
                //NSLog("position: (\(x), \(y)), angle:\(CGFloat(180)*angle/CGFloat(M_PI))")
            }
        }
        let speedConstant : TimeInterval = 20
        
        animateCurtain(starCurtains[0], duration: 4 * speedConstant)
        animateCurtain(starCurtains[1], duration: 3 * speedConstant)
        animateCurtain(starCurtains[2], duration: 2 * speedConstant)
        animateCurtain(starCurtains[3], duration: speedConstant)

    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isToolbarHidden = true
        self.animateStars()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
