//
//  ImageManager.swift
//  GalaxyEvolution
//
//  Created by Dan Rene Rasmussen on 8/10/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

public enum RotationDirection {
    case clockwise
    case counterClockwise
    case none
}

// This class managers the creation of the galaxy images used in the game
open class ImageManager {


    // Determines galaxy image
    open class func imageForGalaxy(_ galaxy: Galaxy, applyColorTint:Bool) -> UIImage {
        if let image = UIImage(named: galaxy.imageName) {
            if applyColorTint {
                let coloredImage = multiplyImageByConstantColor(image, color: UIColor(red: 0.9, green: 0.0, blue: 0.0, alpha: 1.0))
                return coloredImage
            } else {
                return image
            }
        } else {
            return UIImage()
        }
    }

    // Determines image rotation
    open class func rotationDirection(_ imageName : String) -> RotationDirection {
        switch imageName {
        case "Gas_Poor_Disk_Galaxy_2": return .clockwise
        case "Gas_Poor_Disk_Galaxy_app_400": return .clockwise
        case "Gas_Poor_Elliptical_2": return .none
        case "Gas_Poor_Elliptical_Galaxy_3": return .none
        case "Gas_Rich_Disk_Galaxy_2": return .counterClockwise
        case "Gas_Rich_Disk_Galaxy_3": return .counterClockwise
        case "Spiral_Galaxy_2": return .clockwise
        case "Test_Nebula_1": return .none
        default: return .none
        }
    }


}

// Adjusts image tints and hues
private func multiplyImageByConstantColor(_ image:UIImage, color:UIColor) -> UIImage { // based on Apple's Core Image Programming Guide
    let context = CIContext(options:nil)		// FIXME: should create this only once, globally, for efficiency
    let coreImage = CIImage(cgImage: image.cgImage!)

    let hueAdjust = CIFilter(name: "CIHueAdjust")!
    hueAdjust.setValue(coreImage, forKey: kCIInputImageKey)
    hueAdjust.setValue(2.1, forKey: kCIInputAngleKey)

    let cgimage = context.createCGImage(hueAdjust.outputImage!, from: hueAdjust.outputImage!.extent)
    return UIImage(cgImage: cgimage!)
}

