//
//  SegueFromLeft.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 8/1/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

// Class used to create a segue between pages from the left
class SegueFromLeft: UIStoryboardSegue {

    override func perform() {
        let src: UIViewController = self.source
        let dst: UIViewController = self.destination
        let transition: CATransition = CATransition()
        let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.duration = 0.25
        transition.timingFunction = timeFunc
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        src.navigationController!.view.layer.add(transition, forKey: kCATransition)
        src.navigationController!.pushViewController(dst, animated: false)
    }
    
}
