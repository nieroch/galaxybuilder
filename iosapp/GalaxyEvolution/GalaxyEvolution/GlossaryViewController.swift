//
//  GlossaryViewController.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/26/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Class that creates the animated star background
class GlossaryStarCurtain: UIView {

    static let useForScreenShot = false

    init(width: CGFloat, height: CGFloat, starDensity: Float, opacity: Float, color: UIColor) {

        var starSize : CGFloat
        if AchievementsStarCurtain.useForScreenShot {
            starSize = 1.0
        } else {
            starSize = 0.5
        }

        let extendedHeight = 2 * height

        super.init(frame: CGRect(x: 0, y: 0, width: width, height: extendedHeight))

        self.layer.opacity = opacity

        let starCount = Int(starDensity * Float(width * height))

        for _ in 0 ..< starCount {
            let xPos = CGFloat(drand48()) * width
            let yPos = CGFloat(drand48()) * height

            let star = UIView(frame: CGRect(x: xPos, y: yPos, width: starSize, height: starSize))
            star.backgroundColor = color
            self.addSubview(star)

            let mirrorStar = UIView(frame: CGRect(x: xPos, y: yPos + height, width: starSize, height: starSize))
            mirrorStar.backgroundColor = color
            self.addSubview(mirrorStar)
        }

        resetPosition()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func resetPosition() {
        self.frame.origin.y = -frame.height/2
    }
    
}

// View controller for the glossary. There is a tableviewcontroller within this viewcontroller to allow transparency and an animated background.
class GlossaryViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var backToMenuButton: UIButton!
    @IBOutlet weak var glossaryTable: UITableView!

    var overlayView: UIView!
    var alertView: UIView!
    var animator: UIDynamicAnimator!
    var attachmentBehavior : UIAttachmentBehavior!
    var snapBehavior : UISnapBehavior!
    var glossaryItemDescription = UITextView(frame: CGRect(x: 0, y: 60, width: 250, height: 155))
    var glossaryItemDescriptionTitle = UITextView(frame: CGRect(x: 0, y: 0, width: 250, height: 55))

    var starCurtains : [GlossaryStarCurtain] = []
    var listOfGlossaryItems = GlossaryManager.sharedInstance.glossaryItemsList()
    var glossaryItemsToDisplay : [GlossaryItem] = []
    var selectedType = "Glossary"

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.init(white: 0.0, alpha: 1)
        self.navigationController?.isNavigationBarHidden = true

        starCurtains.append(GlossaryStarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.35, color: UIColor.white))
        starCurtains.append(GlossaryStarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.50, color: UIColor.white))
        starCurtains.append(GlossaryStarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.65, color: UIColor.white))
        starCurtains.append(GlossaryStarCurtain(width: self.view.frame.width, height: self.view.frame.height, starDensity: 0.001, opacity: 0.80, color: UIColor.white))

        for curtain in starCurtains {
            self.view.insertSubview(curtain, at: 0)
        }

        //achievementsTitle.textColor = UIColor.init(white: 0.90, alpha: 1)
        backToMenuButton.setTitle("", for: UIControlState())
        let originalImage = UIImage(named: "glyphicons-21-home")
        let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        backToMenuButton.setImage(tintedImage, for: .normal)
        backToMenuButton.tintColor = .white

        backToMenuButton.backgroundColor = UIColor.clear
        backToMenuButton.addTarget(self, action: #selector(GlossaryViewController.returnToMenu), for: UIControlEvents.touchUpInside)

        glossaryTable.backgroundView?.backgroundColor = UIColor(white: 0.80, alpha: 0.15)

        glossaryItemDescription.isUserInteractionEnabled = false
        glossaryItemDescription.font = UIFont(name: "Avenir Next", size: 14)
        glossaryItemDescriptionTitle.isUserInteractionEnabled = false
        glossaryItemDescriptionTitle.font = UIFont(name: "Avenir Next", size: 17)
        glossaryItemDescriptionTitle.textAlignment = .center

        animator = UIDynamicAnimator(referenceView: view)

        //let gesture = UITapGestureRecognizer(target: self, action: #selector(AchievementsViewController.toggle(_:)))
        view.isUserInteractionEnabled = true
        //view.addGestureRecognizer(gesture)

        createOverlay()
        createAlert()

        loadTypes()
    }

    func loadTypes() {
        glossaryItemsToDisplay.removeAll()
        for item in listOfGlossaryItems where item.type == selectedType {
            glossaryItemsToDisplay.append(item)
        }

        glossaryTable.reloadData()
    }

    @IBAction func segmentedAction(sender: MySegmentedControlGlossary) {
        if sender.selectedIndex == 0 {
            selectedType = "Glossary"
        } else {
            selectedType = "Links"
        }
        loadTypes()
    }


    func returnToMenu() {
        performSegue(withIdentifier: "returnToMenuFromGlossary", sender: self)
    }

    func animateStars() {

        func animateCurtain(_ curtain: GlossaryStarCurtain, duration: TimeInterval) {

            curtain.resetPosition()

            UIView.animate(withDuration: duration, delay: 0.0, options: [UIViewAnimationOptions.curveLinear, UIViewAnimationOptions.repeat], animations: {

                curtain.frame.origin.y = 0.0

            }) { (Bool) in
                //completion?()
                //NSLog("position: (\(x), \(y)), angle:\(CGFloat(180)*angle/CGFloat(M_PI))")
            }
        }
        let speedConstant : TimeInterval = 20

        animateCurtain(starCurtains[0], duration: 4 * speedConstant)
        animateCurtain(starCurtains[1], duration: 3 * speedConstant)
        animateCurtain(starCurtains[2], duration: 2 * speedConstant)
        animateCurtain(starCurtains[3], duration: speedConstant)

    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return glossaryItemsToDisplay.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GlossaryCell", for: indexPath)
        cell.textLabel?.textColor = UIColor(white: 0.90, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "Avenir Next", size: 17)
        let glossaryItem = glossaryItemsToDisplay[indexPath.row]
        cell.textLabel?.text = glossaryItem.title

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if selectedType == "Links" {
            UIApplication.shared.open(GlossaryManager.sharedInstance.linkAtIndex(indexPath.row).url!,
                                      options: [:],
                                      completionHandler: nil)
        } else {
            glossaryItemDescription.text = glossaryItemsToDisplay[indexPath.row].description
            glossaryItemDescriptionTitle.text = glossaryItemsToDisplay[indexPath.row].descriptionTitle
            showAlert()
        }
    }

    func updateDescriptionSubview() {
        alertView.addSubview(glossaryItemDescription)
        alertView.addSubview(glossaryItemDescriptionTitle)
    }

    func toggle(_ sender: AnyObject) {
        //navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: true)
    }

    func createOverlay() {
        // Create a gray view and set alpha to 0 so it's invisible
        overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = UIColor.init(white: 0.15, alpha: 0.55)
        overlayView.alpha = 0.0
        view.addSubview(overlayView)
    }

    func createAlert() {
        self.glossaryItemDescription.layer.cornerRadius = 10
        self.glossaryItemDescription.backgroundColor = UIColor.clear
        self.glossaryItemDescription.textColor = UIColor(white: 0.90, alpha: 1)
        self.glossaryItemDescriptionTitle.layer.cornerRadius = 10
        self.glossaryItemDescriptionTitle.backgroundColor = UIColor.clear
        self.glossaryItemDescriptionTitle.textColor = UIColor(white: 0.90, alpha: 1)

        // Here the red alert view is created. It is created with rounded corners and given a shadow around it
        let alertWidth: CGFloat = 250
        let alertHeight: CGFloat = 300
        //let alertViewFrame: CGRect = CGRect(x: 0, y: 0, width: alertWidth, height: alertHeight)
        let alertViewFrame: CGRect = CGRect(x: (UIScreen.main.bounds.width / 2) - (alertWidth / 2), y: (UIScreen.main.bounds.height / 2) - (alertHeight / 2), width: alertWidth, height: alertHeight)
        alertView = UIView(frame: alertViewFrame)
        //alertView.backgroundColor = UIColor(white: 0.95, alpha: 0.90)
        alertView.backgroundColor = UIColor.clear
        alertView.alpha = 0.0
        alertView.layer.cornerRadius = 10
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOffset = CGSize(width: 0, height: 5)
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowRadius = 10.0

        // Create a button and set a listener on it for when it is tapped. Then the button is added to the alert view
        /*let button = UIButton(type: UIButtonType.system) as UIButton
         button.setTitle("Dismiss", for: UIControlState())
         button.titleLabel?.textColor = UIColor(white: 0.50, alpha: 0.90)
         button.backgroundColor = UIColor.clear
         button.frame = CGRect(x: 0, y: 0, width: alertWidth, height: 40.0)
         button.layer.cornerRadius = 10
         button.addTarget(self, action: #selector(AchievementsViewController.dismissAlert(_:)), for: UIControlEvents.touchUpInside)
         */

        //alertView.addSubview(button)
        alertView.addSubview(self.glossaryItemDescription)
        alertView.addSubview(self.glossaryItemDescriptionTitle)
        view.addSubview(alertView)
    }

    func showAlert() {
        // When the alert view is dismissed, I destroy it, so I check for this condition here
        // since if the Show Alert button is tapped again after dismissing, alertView will be nil
        // and so should be created again
        if (alertView == nil) {
            createAlert()
        }

        // I create the pan gesture recognizer here and not in ViewDidLoad() to
        // prevent the user moving the alert view on the screen before it is shown.
        // Remember, on load, the alert view is created but invisible to user, so you
        // don't want the user moving it around when they swipe or drag on the screen.
        createTapGestureRecognizer()

        animator.removeAllBehaviors()

        //let snapBehaviour: UISnapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
        //animator.addBehavior(snapBehaviour)

        // Animate in the overlay
        UIView.animate(withDuration: 0.3, animations: {
            self.overlayView.alpha = 1.0
            self.glossaryTable.alpha = 0.1
            self.backToMenuButton.alpha = 0.1
            self.alertView.alpha = 1.0
        })
    }

    func dismissAlert(_ sender:AnyObject?) {

        animator.removeAllBehaviors()

        let gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [alertView])
        gravityBehaviour.gravityDirection = CGVector(dx: 0.0, dy: 10.0);
        animator.addBehavior(gravityBehaviour)

        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        let itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [alertView])
        itemBehaviour.addAngularVelocity(CGFloat(-(Double.pi/2)), for: alertView)
        animator.addBehavior(itemBehaviour)

        // Animate out the overlay, remove the alert view from its superview and set it to nil
        // If you don't set it to nil, it keeps falling off the screen and when Show Alert button is
        // tapped again, it will snap into view from below. It won't have the location settings we defined in createAlert()
        // And the more it 'falls' off the screen, the longer it takes to come back into view, so when the Show Alert button
        // is tapped again after a considerable time passes, the app seems unresponsive for a bit of time as the alert view
        // comes back up to the screen
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            self.glossaryTable.alpha = 1.0
            self.backToMenuButton.alpha = 1.0
        }, completion: {
            (value: Bool) in
            self.alertView.removeFromSuperview()
            self.alertView = nil
        })
    }

    @IBAction func showAlertView(_ sender: UIButton) {
        showAlert()
    }

    func createTapGestureRecognizer() {
        let dismissTapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AchievementsViewController.handleDismissTap(_:)))
        view.addGestureRecognizer(dismissTapGestureRecognizer)
    }

    func handleDismissTap(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            self.glossaryTable.alpha = 1.0
            self.backToMenuButton.alpha = 1.0
            self.alertView.alpha = 0.0
        }, completion: {
            (value: Bool) in
            self.alertView.removeFromSuperview()
            self.alertView = nil
        })

        self.view.removeGestureRecognizer(sender)
    }

    /*func createGestureRecognizer() {
        let panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(AchievementsViewController.handlePan(_:)))
        view.addGestureRecognizer(panGestureRecognizer)
    }

    func handlePan(_ sender: UIPanGestureRecognizer) {

        if (alertView != nil) {
            let panLocationInView = sender.location(in: view)
            let panLocationInAlertView = sender.location(in: alertView)

            if sender.state == UIGestureRecognizerState.began {
                animator.removeAllBehaviors()

                let offset = UIOffsetMake(panLocationInAlertView.x - alertView.bounds.midX, panLocationInAlertView.y - alertView.bounds.midY);
                attachmentBehavior = UIAttachmentBehavior(item: alertView, offsetFromCenter: offset, attachedToAnchor: panLocationInView)

                animator.addBehavior(attachmentBehavior)
            }
            else if sender.state == UIGestureRecognizerState.changed {
                attachmentBehavior.anchorPoint = panLocationInView
            }
            else if sender.state == UIGestureRecognizerState.ended {
                animator.removeAllBehaviors()

                snapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
                animator.addBehavior(snapBehavior)

                if sender.translation(in: view).y > 100 {
                    dismissAlert(sender)
                }
            }
        }

    }*/

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        self.animateStars()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
}
