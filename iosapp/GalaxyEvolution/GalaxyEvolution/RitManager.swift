//
//  RitManager.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 8/4/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation

// Manager of the RIT info page
open class RitManager {

    open  var links : [(title: String, url: URL?)]

    /// The singleton instance of this class.
    static open let sharedInstance = RitManager()

    fileprivate init() {

        links = [("School of Physics and Astronomy", URL(string: "https://www.rit.edu/science/sopa")),
                 ("SOPA Graduate Studies", URL(string: "http://www.rit.edu/cos/astrophysics")),
                 ("SOPA Programs of Study", URL(string: "https://www.rit.edu/science/sopa/programs")),
                 ("RIT Observatory", URL(string: "https://www.rit.edu/cos/observatory/")),
                 ("Careers in Physics", URL(string: "https://www.rit.edu/science/sopa/careers")),
                 ("Why RIT?", URL(string: "https://www.rit.edu/science/sopa/facts")),
                 ("Physics Research at RIT", URL(string: "https://www.rit.edu/science/sopa/research")),
                 ("RIT Center for Imaging Science", URL(string: "https://www.cis.rit.edu")),
                 ("CIS High School Internship", URL(string: "https://www.cis.rit.edu/outreach/summer-high-school-intern-program")),
        ]

    }

    open func linksCount() -> Int {
        return links.count
    }

    open func linkAtIndex(_ index: Int) -> (title: String, url: URL?) {
        return links[index]
    }
}
