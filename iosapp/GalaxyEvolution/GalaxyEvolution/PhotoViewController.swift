//
//  PhotoViewController.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/8/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import UIKit

// This class is used to set up the view that displays the photo users select from the gallery
class PhotoViewController: UIViewController {

    var overlayView: UIView!
    var alertView: UIView!
    var animator: UIDynamicAnimator!
    var attachmentBehavior: UIAttachmentBehavior!
    var snapBehavior: UISnapBehavior!
    var image: UIImage!
    var imageDescription = UITextView(frame: CGRect(x: 0, y: 45, width: 250, height: 300))
    var imageName: String!
    var aboutButton: UIBarButtonItem!
    var isDescriptionShown = false

    @IBOutlet var photo: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.Photo.image = UIImage(named: "candels_background")
        self.configurePhoto()
        self.view.backgroundColor = UIColor.white

        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(name: "Avenir", size: 16)
        button.sizeToFit()
        self.aboutButton = UIBarButtonItem(customView: button)
        self.aboutButton = UIBarButtonItem(title: "About", style: .plain, target: self, action: #selector(PhotoViewController.showAlert))

        var items = [UIBarButtonItem]()
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
        items.append(aboutButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
        self.toolbarItems = items
        self.navigationController?.setToolbarHidden(false, animated: false)

        // Do any additional setup after loading the view.
        
        animator = UIDynamicAnimator(referenceView: view)

        self.imageDescription.isUserInteractionEnabled = false

        let gesture = UITapGestureRecognizer(target: self, action: #selector(PhotoViewController.toggle(_:)))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)

        createOverlay()
        createAlert()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.title = self.imageName
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Avenir Next", size: 16)!]
        self.navigationController?.navigationBar.titleVerticalPositionAdjustment(for: .default)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setToolbarHidden(true, animated: true)
    }

    func configurePhoto() {
        self.photo.layer.borderWidth = 0.0
        self.photo.layer.borderColor = UIColor.white.cgColor
        self.photo.image = self.image
        self.photo.contentMode = UIViewContentMode.scaleAspectFit
        //self.photo.image = UIImage(named: name)
    }

    func toggle(_ sender: AnyObject) {
        if !isDescriptionShown {
            moveBars()
        }
    }

    func moveBars() {
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)
        if navigationController?.isNavigationBarHidden == false {
            self.navigationController?.setToolbarHidden(false, animated: true)
            self.view.backgroundColor = UIColor.white
        } else {
            self.navigationController?.setToolbarHidden(true, animated: true)
            self.view.backgroundColor = UIColor.black
        }

    }

    override var prefersStatusBarHidden : Bool {
        return navigationController?.isNavigationBarHidden == true
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }

    func createOverlay() {
        // Create a gray view and set its alpha to 0 so it isn't visible
        overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = UIColor.init(white: 0.08, alpha: 0.92)
        overlayView.alpha = 0.0
        view.addSubview(overlayView)
    }

    func createAlert() {
        // Here the red alert view is created. It is created with rounded corners and given a shadow around it
        let alertWidth: CGFloat = 250
        let alertHeight: CGFloat = 300
        let alertViewFrame: CGRect = CGRect(x: (UIScreen.main.bounds.width / 2) - (alertWidth / 2), y: (UIScreen.main.bounds.height / 2) - (alertHeight / 2), width: alertWidth, height: alertHeight)
        alertView = UIView(frame: alertViewFrame)
        alertView.backgroundColor = UIColor.clear
        alertView.alpha = 0.0
        alertView.layer.cornerRadius = 10
        self.imageDescription.textColor = UIColor.white
        self.imageDescription.backgroundColor = UIColor.clear

        // Create a button and set a listener on it for when it is tapped. Then the button is added to the alert view
        let button = UIButton(type: UIButtonType.system) as UIButton
        button.setTitle("Dismiss", for: UIControlState())
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.backgroundColor = UIColor.clear
        button.frame = CGRect(x: 0, y: 0, width: alertWidth, height: 40.0)
        button.addTarget(self, action: #selector(PhotoViewController.dismissAlert(_:)), for: UIControlEvents.touchUpInside)

        alertView.addSubview(button)
        alertView.addSubview(self.imageDescription)
        view.addSubview(alertView)
    }
    
    func showAlert() {
        isDescriptionShown = true

        // When the alert view is dismissed, I destroy it, so I check for this condition here
        // since if the Show Alert button is tapped again after dismissing, alertView will be nil
        // and so should be created again
        if (alertView == nil) {
            createAlert()
        }

        moveBars()
        
        // I create the pan gesture recognizer here and not in ViewDidLoad() to
        // prevent the user moving the alert view on the screen before it is shown.
        // Remember, on load, the alert view is created but invisible to user, so you
        // don't want the user moving it around when they swipe or drag on the screen.

        animator.removeAllBehaviors()
        
        // Animate in the overlay
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 1.0
            self.alertView.alpha = 1.0
        })
        
        // Animate the alert view using UIKit Dynamics.

        // let snapBehaviour: UISnapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
        // animator.addBehavior(snapBehaviour)
    }

    func dismissAlert(_ sender:AnyObject?) {

        isDescriptionShown = false

        animator.removeAllBehaviors()

        moveBars()

        let gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [alertView])
        gravityBehaviour.gravityDirection = CGVector(dx: 0.0, dy: 10.0);
        animator.addBehavior(gravityBehaviour)
        
        // This behaviour is included so that the alert view tilts when it falls, otherwise it will go straight down
        let itemBehaviour: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [alertView])
        itemBehaviour.addAngularVelocity(CGFloat(-(Double.pi/2)), for: alertView)
        animator.addBehavior(itemBehaviour)
        
        // Animate out the overlay, remove the alert view from its superview and set it to nil
        // If you don't set it to nil, it keeps falling off the screen and when Show Alert button is
        // tapped again, it will snap into view from below. It won't have the location settings we defined in createAlert()
        // And the more it 'falls' off the screen, the longer it takes to come back into view, so when the Show Alert button
        // is tapped again after a considerable time passes, the app seems unresponsive for a bit of time as the alert view
        // comes back up to the screen
        UIView.animate(withDuration: 0.4, animations: {
            self.overlayView.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.alertView.removeFromSuperview()
                self.alertView = nil
        })
        
    }
    
    @IBAction func showAlertView(_ sender: UIButton) {
        showAlert()
    }

    /*func createGestureRecognizer() {
        let panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(PhotoViewController.handlePan(_:)))
        view.addGestureRecognizer(panGestureRecognizer)
    }

    func handlePan(_ sender: UIPanGestureRecognizer) {
        
        if (alertView != nil) {
            let panLocationInView = sender.location(in: view)
            let panLocationInAlertView = sender.location(in: alertView)
            
            if sender.state == UIGestureRecognizerState.began {
                animator.removeAllBehaviors()
                
                let offset = UIOffsetMake(panLocationInAlertView.x - alertView.bounds.midX, panLocationInAlertView.y - alertView.bounds.midY);
                attachmentBehavior = UIAttachmentBehavior(item: alertView, offsetFromCenter: offset, attachedToAnchor: panLocationInView)
                
                animator.addBehavior(attachmentBehavior)
            }
            else if sender.state == UIGestureRecognizerState.changed {
                attachmentBehavior.anchorPoint = panLocationInView
            }
            else if sender.state == UIGestureRecognizerState.ended {
                animator.removeAllBehaviors()
                
                snapBehavior = UISnapBehavior(item: alertView, snapTo: view.center)
                animator.addBehavior(snapBehavior)
                
                if sender.translation(in: view).y > 100 {
                    dismissAlert(sender)
                }
            }
        }
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



















