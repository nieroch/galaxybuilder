//
//  VideoView.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 8/8/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit

/*
class VideoView: UIView {

    var name : String
    var type : String
    var videoDescription = ""
    var moviePlayer: MPMoviePlayerController?
    let width : CGFloat = 300.0
    let height : CGFloat = 300.0

    init (name: String, description: String, type: String) {
        self.name = name
        self.type = type
        self.videoDescription = description

        super.init(frame: CGRectMake(0, 0, width, height))

        let pathToEx1 = NSBundle.mainBundle().pathForResource(name, ofType: type)
        let pathURL = NSURL.fileURLWithPath(pathToEx1!)
        moviePlayer = MPMoviePlayerController(contentURL: pathURL)
        if let player = moviePlayer {
            player.view.frame = self.bounds
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            self.addSubview(player.view)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */

}

 */