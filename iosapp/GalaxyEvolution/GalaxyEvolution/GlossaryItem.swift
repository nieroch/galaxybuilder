//
//  GlossaryItem.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/26/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Glossary item object
open class GlossaryItem {
    open var title : String
    open var description = ""
    open var descriptionTitle = ""
    open var type : String

    public init (name: String, description: String, descriptionTitle: String, type: String) {
        self.title = name
        self.description = description
        self.descriptionTitle = descriptionTitle
        self.type = type
    }
    
}
