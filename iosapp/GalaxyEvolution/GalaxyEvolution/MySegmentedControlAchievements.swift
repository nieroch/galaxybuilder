//
//  SegmentedControl.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/17/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Class that creates a custom segmented control object used in the Achievements page
@IBDesignable class MySegmentedControlAchievements : UIControl {

    private var labels = [UILabel]()
    var thumbView = UIView()

    var items : [String] = ["Incomplete", "Complete"] {
        didSet {
            setupLabels()
        }
    }

    var selectedIndex : Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }

    @IBInspectable var selectedLabelColor : UIColor = UIColor.black {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var unselectedLabelColor : UIColor = UIColor(white: 0.90, alpha: 1) {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var  thumbColor : UIColor = UIColor.white {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    func setupView() {
        layer.cornerRadius = frame.height / 2
        layer.borderColor = UIColor.init(white: 0.90, alpha: 0.80).cgColor
        layer.borderWidth = 1

        backgroundColor = UIColor.clear

        setupLabels()

        addIndividualItemConstraints(labels, mainView: self, padding: 0)

        insertSubview(thumbView, at: 0)
    }

    func setupLabels() {
        for label in labels {
            label.removeFromSuperview()
        }

        labels.removeAll(keepingCapacity: true)

        for index in 1...items.count {
            let label = UILabel(frame: CGRect.zero)
            label.text = items[index - 1]
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            //label.textColor = UIColor.init(white: 0.90, alpha: 1)
            label.textColor = index == 1 ? selectedLabelColor : unselectedLabelColor
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            labels.append(label)
        }

        addIndividualItemConstraints(labels, mainView: self, padding: 0)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        var selectedFrame = self.bounds
        let newWidth = selectedFrame.width / CGFloat(items.count)
        selectedFrame.size.width = newWidth
        thumbView.frame = selectedFrame
        thumbView.backgroundColor = thumbColor
        thumbView.layer.cornerRadius = thumbView.frame.height / 2

        let labelHeight = self.bounds.height
        let labelWidth = self.bounds.width / CGFloat(labels.count)

        for index in 0...labels.count - 1 {
            var label = labels[index]

            let xPosition = CGFloat(index) * labelWidth
            label.frame = CGRect(origin: CGPoint(x: xPosition, y: 0), size: CGSize(width: labelWidth, height: labelHeight))
        }
    }

    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)

        var calculatedIndex : Int?
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }

        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            //NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.MySegmentedControl.StateUpdated), object: nil))
            sendActions(for: .valueChanged)
        }

        return false
    }

    func displayNewSelectedIndex() {

        for (index, item) in labels.enumerated() {
            item.textColor = unselectedLabelColor
        }

        let label = labels[selectedIndex]
        label.textColor = selectedLabelColor

        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: [], animations: {

            self.thumbView.frame = label.frame

        }, completion: nil)

        self.thumbView.frame = label.frame
    }

    func addIndividualItemConstraints(_ items: [UIView], mainView: UIView, padding: CGFloat) {

        //let constraints = mainView.constraints

        for (index, button) in items.enumerated() {

            let topConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)

            let bottomConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)

            var rightConstraint : NSLayoutConstraint!

            if index == items.count - 1 {

                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: -padding)

            }else{

                let nextButton = items[index+1]
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: nextButton, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: -padding)
            }


            var leftConstraint : NSLayoutConstraint!

            if index == 0 {

                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: padding)

            }else{

                let prevButton = items[index-1]
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: prevButton, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: padding)

                let firstItem = items[0]

                var widthConstraint = NSLayoutConstraint(item: button, attribute: .width, relatedBy: NSLayoutRelation.equal, toItem: firstItem, attribute: .width, multiplier: 1.0  , constant: 0)

                mainView.addConstraint(widthConstraint)
            }

            mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
        }
    }

    func setSelectedColors() {
        for item in labels {
            item.textColor = unselectedLabelColor
        }

        if labels.count > 0 {
            labels[0].textColor = selectedLabelColor
        }

        thumbView.backgroundColor = thumbColor
    }

}
















