//
//  Universe.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/24/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


/// This is a singleton class representing the universe.
open class Universe {
    
    public enum universeDensityCategory {
        case high
        case medium
        case low
    }

    /// The galaxy that is controlled by the user.
    open var userGalaxy : Galaxy!

    /// A list of all the galaxies currently in the universe.
    fileprivate(set) open var galaxies : [Galaxy] = []

    fileprivate var updateTimer : Timer?

    /// The interval used for updating the user galaxy properties [seconds].
    open let updateInterval : TimeInterval = 1.0

    fileprivate let minGameDuration : Double = 100
    fileprivate let maxGameDuration : Double = 5000
    fileprivate var gameDuration : Double = 2550    // 1000 seconds

    /// The life span of the universe [years].
    open let universeLifespan : Double = 14000000000 /// 14 billion years

    var secondEquivalence : Int // years per second

    /// The current age of the universe [millions of years]
    open var universeAge = 0.0 // Age in millions of years

    let universeLifespanMillions = 14000.0

    /// The fastest speed that a galaxy can move accross the screen [points per second]
    open let universalSpeedLimit = 140.0 // points per second

    /// The density of galaxies in the universe [galaxies per 100,000 square points]
    open var galaxyDensity : Double = 2.0 // Galaxies per 100,000 square points

    fileprivate(set) var userVisibleWidth : Double = 0.0
    fileprivate(set) var userVisibleHeight : Double = 0.0
    fileprivate var neighborhoodWidth : Double = 0.0
    fileprivate var neighborhoodHeight : Double = 0.0
    fileprivate var vicinityWidth : Double = 0.0
    fileprivate var vicinityHeight : Double = 0.0
    fileprivate var gameIsPaused : Bool = false
    fileprivate let trulyRandom : Bool = false

    /// The singleton instance of this class.
    static open let universe = Universe()
    
    fileprivate init() {
        secondEquivalence = Int(universeLifespan/gameDuration)
        Qalog("One second of game time is equivalent to \(secondEquivalence) years")

        if trulyRandom {
            srand48(time(UnsafeMutablePointer(bitPattern: 0)))
        }
    }

    /// Removes the galaxy from the universe.
    open func removeGalaxy(_ galaxyToRemove: Galaxy) {

        let index = galaxies.index { (g : Galaxy) -> Bool in
            return g === galaxyToRemove
        }

        if let index = index {
            self.galaxies.remove(at: index)
        }
    }

    /// Adds a galaxy to the universe.
    open func addGalaxy(_ galaxyToAdd: Galaxy) {
        galaxies.append(galaxyToAdd)
    }

    /// Sets up the initial universe. Called when a new game begins.
    @objc func startEvolution() {
        assert(galaxies.count == 0)
        userGalaxy = Galaxy(mass: 10000, blackHoleMass: 4, stellarMass: 600, shape: Galaxy.ShapeCategory.spiral)
        //userGalaxy.printGalaxy("User Galaxy")
        updateTimer = Timer.scheduledTimer(timeInterval: updateInterval, target: self, selector: #selector(Universe.updateUniverse), userInfo: nil, repeats: true)
        userGalaxy.x = 0.0
        userGalaxy.y = 0.0
        self.galaxies.append(userGalaxy)
    }

    /// Resets the universe. This is called when the user returns to the menu.
    @objc func reset() {
        self.updateTimer?.invalidate()
        self.updateTimer = nil
        self.galaxies.removeAll()
        self.universeAge = 0.0
        self.gameDuration = -2550
    }

    /// Updates how much one seconds equals in years
    func updateSecondEquivalence(_ sliderValue: Double) {
        self.gameDuration = -1 * sliderValue
        self.secondEquivalence = Int(self.universeLifespan/self.gameDuration)
    }

    /**
     The scene size should be independent of the device screen size, as much as possible, so that the region of the universe that the user can see is about the same on all devices.
     But the scene size must be adjusted to fit the device aspect ratio.
     */
    open func setDimensionsForScreenDimensions(screenWidth:Double, screenHeight:Double) {
        self.userVisibleWidth = 320		// Can be anything, but this is what it used to be with earlier code
        self.userVisibleHeight = self.userVisibleWidth * screenHeight / screenWidth	// match aspect ratio
        self.setNeighborhoodDimensions()
    }

    /**
     Sets the size of the neighborhood of the user galaxy. This is the region where objects (galaxies) are not modified, such that the user has a sense that the immediate surroundings are not randomly changing.
     The dimensions are in device screen points, and should be at least the size of user-visible scene to prevent the user from seeing objects suddenly appear or disappear.
     */
    open func setNeighborhoodDimensions() {
        self.neighborhoodWidth = 2.25 * self.userVisibleWidth
        self.neighborhoodHeight = 2.25 * self.userVisibleHeight
        Qalog("Neighborhood dimensions: \(self.neighborhoodWidth) x \(self.neighborhoodHeight)")
        self.setVicinityDimensions()
    }

    /**
     This sets the vicinity dimensions. For example, in the x direction, the vicinity width is set to be the neighborhood width plus twice the max distance that the user galaxy could move between updates.
     */
    func setVicinityDimensions() {
        let maxDisplacement = self.universalSpeedLimit * self.updateInterval
        self.vicinityWidth = maxDisplacement + self.neighborhoodWidth
        self.vicinityHeight = maxDisplacement + self.neighborhoodHeight
        Qalog("Vicinity dimensions: \(self.vicinityWidth) x \(self.vicinityHeight)")
    }

    /// For debugging purposes, this function prints out key information about the universe.
    open func printUniverse() {
        print("Number of objects in universe (other than user): \(galaxies.count)")
        userGalaxy.printGalaxy("User Galaxy")

        for galaxy in galaxies {
            galaxy.printGalaxy(nil)
        }

    }

    /// Pauses the universe. Timers and updates are paused.
    open func pause() {
        NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Universe.ObjectsPaused), object: nil))
        self.updateTimer?.invalidate()
        self.updateTimer = nil
        self.gameIsPaused = true
    }

    /// Resumes the universe. Timers and updates are resumed.
    open func resume() {
        NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Universe.ObjectsResumed), object: nil))
        if self.gameIsPaused {
            updateTimer = Timer.scheduledTimer(timeInterval: updateInterval, target: self, selector: #selector(Universe.updateUniverse), userInfo: nil, repeats: true)
            self.gameIsPaused = false
        }
    }

    /**
     Returns an object type (galaxy or other type) selected according to the probabilities for the given universe age.
     */
    open func typeForNewSpaceObject(_ universeAge: Double) -> Galaxy.GalaxyType {
        var cumulatedProbabilities : [Galaxy.GalaxyType: Double] = [:]      // this should be updated as the universe ages

        if universeAge < 6000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else if universeAge < 8000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else if universeAge < 9000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else if universeAge < 12000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else if universeAge < 13000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else if universeAge < 14000 {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        } else {
            cumulatedProbabilities[.heavyGasRichDisk] =                 0.03       // 03%
            cumulatedProbabilities[.gasRichDisk] =                      0.23       // 20%
            cumulatedProbabilities[.lightGasRichDisk] =                 0.43       // 20%
            cumulatedProbabilities[.heavyGasPoorDisk] =                 0.45       // 02%
            cumulatedProbabilities[.gasPoorDisk] =                      0.55       // 10%
            cumulatedProbabilities[.lightGasPoorDisk] =                 0.67       // 12%
            cumulatedProbabilities[.heavyGasPoorElliptical] =           0.70       // 03%
            cumulatedProbabilities[.gasPoorElliptical] =                0.80       // 10%
            cumulatedProbabilities[.lightGasPoorElliptical] =           1.00       // 20%
        }

        let p = drand48()

        if p < cumulatedProbabilities[.heavyGasRichDisk] {
            return .heavyGasRichDisk
        } else if p < cumulatedProbabilities[.gasRichDisk] {
            return .gasRichDisk
        } else if p < cumulatedProbabilities[.lightGasRichDisk] {
            return .lightGasRichDisk
        } else if p < cumulatedProbabilities[.heavyGasPoorDisk] {
            return .heavyGasPoorDisk
        } else if p < cumulatedProbabilities[.gasPoorDisk] {
            return .gasPoorDisk
        } else if p < cumulatedProbabilities[.heavyGasPoorDisk] {
            return .lightGasPoorDisk
        } else if p < cumulatedProbabilities[.heavyGasPoorElliptical] {
            return .heavyGasPoorElliptical
        } else if p < cumulatedProbabilities[.gasPoorElliptical] {
            return .gasPoorElliptical
        } else if p < cumulatedProbabilities[.lightGasPoorElliptical] {
            return .lightGasPoorElliptical
        } else {
            return .lightGasPoorElliptical
        }
    }

    /// Function that returns a galaxy object of the requested type
    func galaxyOfType(_ type: Galaxy.GalaxyType) -> Galaxy {
        if type == .heavyGasRichDisk {
            return Galaxy(mass: 100000, blackHoleMass: 4, stellarMass: 50000, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .gasRichDisk {
            return Galaxy(mass: 10000, blackHoleMass: 3, stellarMass: 5000, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .lightGasRichDisk {
            return Galaxy(mass: 900, blackHoleMass: 2, stellarMass: 450, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .heavyGasPoorDisk {
            return Galaxy(mass: 100000, blackHoleMass: 4, stellarMass: 90000, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .gasPoorDisk {
            return Galaxy(mass: 10000, blackHoleMass: 3, stellarMass: 9000, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .lightGasPoorDisk {
            return Galaxy(mass: 900, blackHoleMass: 2, stellarMass: 800, shape: Galaxy.ShapeCategory.spiral)
        } else if type == .heavyGasPoorElliptical {
            return Galaxy(mass: 100000, blackHoleMass: 4, stellarMass: 90000, shape: Galaxy.ShapeCategory.elliptical)
        } else if type == .gasPoorElliptical {
            return Galaxy(mass: 10000, blackHoleMass: 3, stellarMass: 9000, shape: Galaxy.ShapeCategory.elliptical)
        } else {
            return Galaxy(mass: 900, blackHoleMass: 2, stellarMass: 800, shape: Galaxy.ShapeCategory.elliptical)
        }
    }

    /// Updates all the instance variables on the universe and adds and remove galaxies
    @objc open func updateUniverse() {
        self.updateGalaxyProperties()
        self.universeAge += self.updateInterval * Double(self.secondEquivalence) / 1000000
        NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Universe.AgeUpdated), object: nil))

        let removedGalaxies = self.clearRegion(self.userGalaxy.x, centerY: self.userGalaxy.y, insideWidth: self.neighborhoodWidth, insideHeight: self.neighborhoodHeight)
        let addedGalaxies = self.createGalaxies(self.userGalaxy.x, centerY: self.userGalaxy.y, insideWidth: self.neighborhoodWidth, insideHeight: self.neighborhoodHeight, outsideWidth: self.vicinityWidth, outsideHeight: self.vicinityHeight)

        // Send notifications about added and removed galaxies
        let userInfo = [GEKeys.RemovedSpaceObjectsKey : removedGalaxies, GEKeys.AddedSpaceObjectsKey : addedGalaxies]
        let notification = Notification(name: Notification.Name(GENotification.Universe.SpaceObjectsUpdated), object: nil, userInfo: userInfo)
        NotificationCenter.default.post(notification)
    }

    /**
     This updates the user galaxy. It is automatically called periodically to account for gas accretion, star formation, etc.
     */
    open func updateGalaxyProperties() {
        let timeDelta = self.updateInterval * Double(self.secondEquivalence)
        // This is taking into account the star formation rate
        let averageNewStarMass = 0.000001 // Our mass unit is 1 million solar masses, and this is assuming average new star mass is roughly 1 solar mass (this could be wrong)
        var massChange = timeDelta * self.userGalaxy.starFormationRate * averageNewStarMass
        massChange = min(massChange, self.userGalaxy.gasMass)
        self.userGalaxy.stellarMass += massChange
        self.userGalaxy.gasMass -= massChange

        // This is taking into account the gas accretion rate
        self.userGalaxy.gasMass += self.userGalaxy.gasAccretionRate * timeDelta * 0.000001 // I multiply by 0.000001 because the unit for gas mass in million solar masses
    }

    /**
     This function creates galaxies in the region between the inside rectangle (usually the neighborhood) and the outside rectangle (usually the vicinity). It makes sure there are no intersections between the galaxies.
     
     Parameters:
     - centerX: The x center of the region. Normally should be be coordinate of the user galaxy.
     - centerY: The y center of the region. Normally should be be coordinate of the user galaxy.

     */
    open func createGalaxies(_ centerX: Float, centerY: Float, insideWidth: Double, insideHeight: Double, outsideWidth: Double, outsideHeight: Double) -> [Galaxy]{
        let usableArea = (outsideWidth * outsideHeight) - (insideWidth * insideHeight)
        let numberOfGalaxies = Int(self.galaxyDensity * usableArea / 100000)
        let minimumDistanceBetweenGalaxies : Float = 100
        var galaxiesAdded : [Galaxy] = []

        for _ in 0 ..< numberOfGalaxies {

            let newGalaxyType = self.typeForNewSpaceObject(self.universeAge)
            let newGalaxy = self.galaxyOfType(newGalaxyType)

            var intersects = true

            while (intersects) {
                var xPos : Float = 0.0
                var yPos : Float = 0.0

                var positionHasBeenSet = false

                while !positionHasBeenSet {
                    let y = Float(drand48()-0.5) * Float(outsideHeight)
                    let x = Float(drand48()-0.5) * Float(outsideWidth)

                    if abs(x) > (Float(insideWidth) / 2) {
                        xPos = x + centerX
                        yPos = y + centerY
                        positionHasBeenSet = true
                    } else if abs(y) > (Float(insideHeight) / 2) {
                        xPos = x + centerX
                        yPos = y + centerY
                        positionHasBeenSet = true
                    }
                }

                newGalaxy.x = xPos
                newGalaxy.y = yPos

                intersects = false

                for galaxy in galaxies {

                    if abs(galaxy.x - newGalaxy.x) < minimumDistanceBetweenGalaxies && abs(galaxy.y - newGalaxy.y) < minimumDistanceBetweenGalaxies {
                        intersects = true
                        break
                    }
                }
            }
            self.galaxies.append(newGalaxy)
            galaxiesAdded.append(newGalaxy)
            
        }
        //Qalog("Galaxies created: \(galaxiesAdded.count) in vicinity of (\(centerX), \(centerY)), Galaxy count: \(self.galaxies.count)")
        return galaxiesAdded
    }

    /**
     This function clears all galaxies within the region between the inside rectangle (usually the neighborhood) and the outside rectangle (usually the vicinity).
     */
    open func clearRegion(_ centerX: Float, centerY: Float, insideWidth: Double, insideHeight: Double) -> [Galaxy] {
        var galaxiesRemoved : [Galaxy] = []

        for i in (0 ..< self.galaxies.count).reversed() {
            if abs(self.galaxies[i].x - centerX) > (Float(insideWidth) / 2) {
                //Qalog("Center: (\(centerX), \(centerY))  Inside Width: \(insideWidth)  Inside Height: \(insideHeight)  Galaxy X: \(self.galaxies[i].x)")
                galaxiesRemoved.append(galaxies[i])
                self.galaxies.remove(at: i)
            } else if abs(self.galaxies[i].y - centerY) > (Float(insideHeight) / 2) {
                //Qalog("Center: (\(centerX), \(centerY))  Inside Width: \(insideWidth)  Inside Height: \(insideHeight)  Galaxy Y: \(self.galaxies[i].y)")
                galaxiesRemoved.append(galaxies[i])
                self.galaxies.remove(at: i)
            }
        }
        //Qalog("Galaxies removed: \(galaxiesRemoved.count), Galaxy count: \(self.galaxies.count)")
        return galaxiesRemoved
    }

    fileprivate func distanceSquaredBetweenGalaxies(_ galaxy1: Galaxy, galaxy2: Galaxy) -> Float {
        
        let dx = galaxy1.x - galaxy2.x
        let dy = galaxy1.y - galaxy2.y
        
        let distanceSquared = dx * dx + dy * dy
        
        return distanceSquared
    }
    
    
    
    
    
    
    
    
}














