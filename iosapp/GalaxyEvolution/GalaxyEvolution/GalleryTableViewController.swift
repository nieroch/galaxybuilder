//
//  GalleryTableViewController.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/8/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import UIKit

// TableViewController for the gallery
class GalleryTableViewController: UITableViewController {

    var listOfPhotos = GalleryManager.sharedInstance.photoList()
    var listOfVideos = GalleryManager.sharedInstance.videoList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Avenir Next", size: 16)!]

        // Uncomment the following vare to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    //override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //return "Hubble Telescope Images"
    //}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listOfPhotos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell : CustomCell = CustomCell(style: UITableViewCellStyle.Default, reuseIdentifier: "PhotoCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath)

        cell.textLabel?.text = listOfPhotos[indexPath.row].name
        cell.imageView?.image = listOfPhotos[indexPath.row].image

        let itemSize = CGSize(width: 45, height: 45)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        let imageRect = CGRect(x: 0.0, y: 0.0, width: itemSize.width, height: itemSize.height)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem

        if let pvc = segue.destination as? PhotoViewController {
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPath(for: cell) {
                    let row = indexPath.row
                    
                    switch row {
                    case 0:
                        pvc.image = listOfPhotos[0].image
                        pvc.imageDescription.text = listOfPhotos[0].description
                        pvc.imageName = listOfPhotos[0].name
                    case 1:
                        pvc.image = listOfPhotos[1].image
                        pvc.imageDescription.text = listOfPhotos[1].description
                        pvc.imageName = listOfPhotos[1].name
                    case 2:
                        pvc.image = listOfPhotos[2].image
                        pvc.imageDescription.text = listOfPhotos[2].description
                        pvc.imageName = listOfPhotos[2].name
                    case 3:
                        pvc.image = listOfPhotos[3].image
                        pvc.imageDescription.text = listOfPhotos[3].description
                        pvc.imageName = listOfPhotos[3].name
                    case 4:
                        pvc.image = listOfPhotos[4].image
                        pvc.imageDescription.text = listOfPhotos[4].description
                        pvc.imageName = listOfPhotos[4].name
                    case 5:
                        pvc.image = listOfPhotos[5].image
                        pvc.imageDescription.text = listOfPhotos[5].description
                        pvc.imageName = listOfPhotos[5].name
                    case 6:
                        pvc.image = listOfPhotos[6].image
                        pvc.imageDescription.text = listOfPhotos[6].description
                        pvc.imageName = listOfPhotos[6].name
                    case 7:
                        pvc.image = listOfPhotos[7].image
                        pvc.imageDescription.text = listOfPhotos[7].description
                        pvc.imageName = listOfPhotos[7].name
                    case 8:
                        pvc.image = listOfPhotos[8].image
                        pvc.imageDescription.text = listOfPhotos[8].description
                        pvc.imageName = listOfPhotos[8].name
                    case 9:
                        pvc.image = listOfPhotos[9].image
                        pvc.imageDescription.text = listOfPhotos[9].description
                        pvc.imageName = listOfPhotos[9].name
                    case 10:
                        pvc.image = listOfPhotos[10].image
                        pvc.imageDescription.text = listOfPhotos[10].description
                        pvc.imageName = listOfPhotos[10].name
                    case 11:
                        pvc.image = listOfPhotos[11].image
                        pvc.imageDescription.text = listOfPhotos[11].description
                        pvc.imageName = listOfPhotos[11].name
                    case 12:
                        pvc.image = listOfPhotos[12].image
                        pvc.imageDescription.text = listOfPhotos[12].description
                        pvc.imageName = listOfPhotos[12].name
                    case 13:
                        pvc.image = listOfPhotos[13].image
                        pvc.imageDescription.text = listOfPhotos[13].description
                        pvc.imageName = listOfPhotos[13].name
                    case 14:
                        pvc.image = listOfPhotos[14].image
                        pvc.imageDescription.text = listOfPhotos[14].description
                        pvc.imageName = listOfPhotos[14].name
                    case 15:
                        pvc.image = listOfPhotos[15].image
                        pvc.imageDescription.text = listOfPhotos[15].description
                        pvc.imageName = listOfPhotos[15].name
                    case 16:
                        pvc.image = listOfPhotos[16].image
                        pvc.imageDescription.text = listOfPhotos[16].description
                        pvc.imageName = listOfPhotos[16].name
                    case 17:
                        pvc.image = listOfPhotos[17].image
                        pvc.imageDescription.text = listOfPhotos[17].description
                        pvc.imageName = listOfPhotos[17].name
                    case 18:
                        pvc.image = listOfPhotos[18].image
                        pvc.imageDescription.text = listOfPhotos[18].description
                        pvc.imageName = listOfPhotos[18].name
                    case 19:
                        pvc.image = listOfPhotos[19].image
                        pvc.imageDescription.text = listOfPhotos[19].description
                        pvc.imageName = listOfPhotos[19].name
                    case 20:
                        pvc.image = listOfPhotos[20].image
                        pvc.imageDescription.text = listOfPhotos[20].description
                        pvc.imageName = listOfPhotos[20].name
                    case 21:
                        pvc.image = listOfPhotos[21].image
                        pvc.imageDescription.text = listOfPhotos[21].description
                        pvc.imageName = listOfPhotos[21].name
                    case 22:
                        pvc.image = listOfPhotos[22].image
                        pvc.imageDescription.text = listOfPhotos[22].description
                        pvc.imageName = listOfPhotos[22].name
                    case 23:
                        pvc.image = listOfPhotos[23].image
                        pvc.imageDescription.text = listOfPhotos[23].description
                        pvc.imageName = listOfPhotos[23].name
                    case 24:
                        pvc.image = listOfPhotos[24].image
                        pvc.imageDescription.text = listOfPhotos[24].description
                        pvc.imageName = listOfPhotos[24].name
                    case 25:
                        pvc.image = listOfPhotos[25].image
                        pvc.imageDescription.text = listOfPhotos[25].description
                        pvc.imageName = listOfPhotos[25].name

                    //case 10:
                        //pvc.photo.addSubview(listOfVideos[0])
                        //pvc.imageDescription.text = (listOfVideos[0].videoDescription)
                    default:
                        break
                    }
                }
            }
        }

    }

}











