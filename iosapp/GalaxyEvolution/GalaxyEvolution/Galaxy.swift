//
//  Galaxy.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/20/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//


import Foundation

/**
 This is a model class for a galaxy. This class is used to represent both the *user galaxy*, which is the galaxy controlled by the user, and all other galaxies in the universe.

 ##Important note##
 Even though the class is named *galaxy*, it may also be used to represent other objects in the universe, such as pre-galactic clouds of materials.

 */
open class Galaxy {
    
    fileprivate enum GasConcentrationCategory {
        case poor
        case rich
    }
    
    fileprivate enum RelativeMassCategory {
        case equal
        case heavier
        case lighter
        case muchLighter
    }

    fileprivate enum AbsoluteMassCategory { //FIXME: Will have to see if what qualifies as "massive" or "light" changed from spiral to elliptical galaxies. Will have to make a category for each
        case heavy
        case average
        case light
    }

    fileprivate enum GalacticNucleiCategory {
        case active
        case inactive
    }

    fileprivate enum SpecialCategory {
        case superMassive
        case highStarFormationRate
        case highStellarMass
        case noSpecial
    }

    /**
     This enumerated type is used to categorize space objects. It should be expanded to cover any type of object, but there is currently only galaxy types.
     */
    public enum GalaxyType {
        case lightGasRichDisk
        case lightGasPoorDisk
        case lightGasPoorElliptical
        case gasRichDisk
        case gasPoorDisk
        case gasPoorElliptical
        case heavyGasRichDisk
        case heavyGasPoorDisk
        case heavyGasPoorElliptical

        case superMassiveGasRichSpiral
        case massiveGasRichSpiral   //FIXME: The words massive, normal, light, very light, refer to total mass. Once we determine how to find physical size, we will add that
        case normalGasRichSpiral
        case lightGasRichSpiral
        case veryLightGasRichSpiral

        case superMassiveGasPoorElliptical
        case massiveGasPoorElliptical
        case normalGasPoorElliptical
        case veryLightGasPoorElliptical

        /**
         - Returns: A list of all the categories.
         */
        static func allTypes() -> [GalaxyType] {
            return [gasRichDisk, gasPoorDisk, gasPoorElliptical, superMassiveGasRichSpiral, massiveGasRichSpiral, normalGasRichSpiral, lightGasRichSpiral, veryLightGasRichSpiral,
            superMassiveGasPoorElliptical, massiveGasPoorElliptical, normalGasPoorElliptical, lightGasPoorElliptical, veryLightGasPoorElliptical]
        }

        /**
         - Returns: English name for the galaxy category.
         */
        public func name() -> String {
            switch self {
            case .lightGasRichDisk: return "Light gas rich disk"
            case .lightGasPoorDisk: return "Light gas poor disk"
            case .lightGasPoorElliptical: return "Light gas poor elliptical"

            case .gasRichDisk: return "Gas rich disk"
            case .gasPoorDisk: return "Gas poor disk"
            case .gasPoorElliptical: return "Gas poor elliptical"

            case .heavyGasRichDisk: return "Heavy gas rich disk"
            case .heavyGasPoorDisk: return "Heavy gas poor disk"
            case .heavyGasPoorElliptical: return "Heavy gas poor elliptical"

            case .superMassiveGasRichSpiral: return "Super massive gas rich spiral"
            case .massiveGasRichSpiral: return "Massive gas rich spiral"
            case .normalGasRichSpiral: return "Normal gas rich spiral"
            case .lightGasRichSpiral: return "Light gas rich spiral"
            case .veryLightGasRichSpiral: return "Very light gas rich spiral"

            case .superMassiveGasPoorElliptical: return "Super massive gas poor elliptical"
            case .massiveGasPoorElliptical: return "Massive gas poor elliptical"
            case .normalGasPoorElliptical: return "Normal gas poor elliptical"
            case .veryLightGasPoorElliptical: return "Very light gas poor elliptical"
            }
        }
    }

    /// Galaxy category based on shape alone.
    public enum ShapeCategory {
        case spiral
        case elliptical
    }

    fileprivate static var galaxyCount : Int = 0

    //let radius : Float = 0.0 // Units is probably lightyears

    /// Gas threshold determines whether the galaxy is gas-rich or gas-poor.
    open let gasThreshold = 20.0

    /// The x location of the galaxy.
    open var x : Float = 0

    /// The y location of the galaxy.
    open var y : Float = 0

    /// The total mass of the galaxy, calculated from component masses. The mass unit is 10^6 solar masses
    open var totalMass : Double {
        get {
            return gasMass + blackHoleMass + stellarMass
        }
    }

    /// The mass of the black hole within the galaxy. The mass unit is 10^6 solar masses
    open var blackHoleMass : Double {
        didSet {
            self.categoryIsDirty = true
        }
    }

    /// The total mass of stars in the galaxy. The mass unit is 10^6 solar masses
    open var stellarMass : Double {
        didSet {
            self.categoryIsDirty = true
        }
    }

    /// The total mass of gas in the galaxy. The mass unit is 10^6 solar masses
    open var gasMass : Double {
        didSet {
            self.categoryIsDirty = true
        }
    }

    // Collision Bools. Bools that represent whether or not the user has collided with a certain type of galaxy before
    var haveCollidedWithGasRichDisk = false
    var haveCollidedWithGasPoorDisk = false
    var haveCollidedWithGasPoorElliptical = false

    fileprivate var shapeCategory : ShapeCategory

    /// The rate at which gas is accreted. Unit is solar masses per year.
    open var gasAccretionRate : Double = 350.0
    fileprivate var gasAccretionRateMax : Double = 700.0

    /// Percentage gas of the total mass.
    open var gasLevel : Double {
        get {
            return 100 * gasMass/totalMass
        }
    }

    /**
     The category of the galaxy is automatically calculated based on current properties.
     */
    open var category : GalaxyType {
        get {
            if categoryIsDirty {
                let newCategory = self.findCategory()
                let oldCategory = self._category
                self._category = newCategory

                if oldCategory != newCategory {
                    self._imageName = nil
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Universe.UserGalaxyCategoryChanged), object: nil)) //FIXME: We don't know that the user galaxy is changed here
                }
            }
            return _category
        }
    }
    fileprivate var _category : GalaxyType = .gasRichDisk
    fileprivate var categoryIsDirty : Bool = true

    /// An opaque reference to the corresponding sprite. This is set and used only by the game scene, such that sprites can be removed when galaxies are removed.
    internal var sprite : AnyObject?

    /// An image is associated with the galaxy according to its type. But there may be several images available for each type, so this variable holds the name of the image that is used for this particular galaxy.
    open var imageName: String {
        get {
            if _imageName == nil {
                _imageName = self.imageNameForGalaxyType()
            }
            return _imageName!
        }
    }
    fileprivate var _imageName : String? = nil

    /// The rotation direction of the galaxy is automatically be set such that it matches the image that is associated with the galaxy.
    open var rotation : RotationDirection = .none

    /**
     The SRF units is stars per year.
     for a gas rich disk the SRF is
     ````
     SFR = -24.365 + (17.25 * log10(stellarMass)) + (-0.175 * log10(stellarMass) * log10(stellarMass))
     ````
     and for other cases ````SFR = 5.0````.
     */
    open var starFormationRate : Double {
        get {
            var SFR : Double
            switch self.category {
            case .gasRichDisk:
                SFR = -24.365 + (17.25 * log10(stellarMass)) + (-0.175 * log10(stellarMass) * log10(stellarMass))
            case .gasPoorDisk:
                SFR = 5.0
            case .gasPoorElliptical:
                SFR = 5.0
            default:
                SFR = 5.0
            }
            return SFR
        }
    }

    fileprivate let sfrSlope = 0.05
    fileprivate let blackHoleScale = 0.01
    fileprivate let stellarScale = 1.20
    fileprivate let stellarInteractScale = 1.10
    
    fileprivate func imageNameForGalaxyType() -> String {
        switch self.category {
        case .heavyGasRichDisk:
            //let gasRichDiskImages : [String] = ["Spiral_Galaxy_2", "Gas_Rich_Disk_Galaxy_2", "Gas_Rich_Disk_Galaxy_3"]
            //var index = Int(drand48() * Double(gasRichDiskImages.count))
            //if index == 3 {
            //    index = 2
            //}
            //return gasRichDiskImages[index]
            return "Spiral_Galaxy_2"
        case .gasRichDisk:
            return "Gas_Rich_Disk_Galaxy_2"
        case .lightGasRichDisk:
            return "Gas_Rich_Disk_Galaxy_3"

        case .heavyGasPoorDisk:
            //let gasPoorDiskImages : [String] = ["Gas_Poor_Disk_Galaxy_app_400", "Gas_Poor_Disk_Galaxy_2"]
            //var index = Int(drand48() * Double(gasPoorDiskImages.count))
            //if index == 2 {
            //    index = 1
            //}
            return "Gas_Poor_Disk_Galaxy_2"
        case .gasPoorDisk:
            return "Gas_Poor_Disk_Galaxy_app_400"
        case .lightGasPoorDisk:
            return "Gas_Poor_Disk_Galaxy_app_400"

        case .heavyGasPoorElliptical:
            //let gasPoorEllipticalImages : [String] = ["Gas_Poor_Elliptical_app_400", "Gas_Poor_Elliptical_2", "Gas_Poor_Elliptical_Galaxy_3"]
            //var index = Int(drand48() * Double(gasPoorEllipticalImages.count))
            //if index == 3 {
            //    index = 2
            //}
            return "Gas_Poor_Elliptical_2"
        case .gasPoorElliptical:
            let gasPoorEllipticalImages : [String] = ["Gas_Poor_Elliptical_app_400", "Gas_Poor_Elliptical_2", "Gas_Poor_Elliptical_Galaxy_3"]
            var index = Int(drand48() * Double(gasPoorEllipticalImages.count))
            if index == 3 {
                index = 2
            }
            return "Gas_Poor_Elliptical_2"
        case .lightGasPoorElliptical:
            let gasPoorEllipticalImages : [String] = ["Gas_Poor_Elliptical_app_400", "Gas_Poor_Elliptical_2", "Gas_Poor_Elliptical_Galaxy_3"]
            var index = Int(drand48() * Double(gasPoorEllipticalImages.count))
            if index == 3 {
                index = 2
            }
            return "Gas_Poor_Elliptical_Galaxy_3"


        case .superMassiveGasRichSpiral: return "Spiral_Galaxy_2"
        case .massiveGasRichSpiral: return "Spiral_Galaxy_2"
        case .normalGasRichSpiral: return "Spiral_Galaxy_2"
        case .lightGasRichSpiral: return "Spiral_Galaxy_2"
        case .veryLightGasRichSpiral: return "Spiral_Galaxy_2"

        case .superMassiveGasPoorElliptical: return "Test_Nebula_1"
        case .massiveGasPoorElliptical: return "Test_Nebula_1"
        case .normalGasPoorElliptical: return "Test_Nebula_1"
        case .veryLightGasPoorElliptical: return "Test_Nebula_1"
        }
    }

    /**
     Creates a new galaxy with the specified mass components and shape category.
     */
    public init(mass: Double, blackHoleMass: Double, stellarMass : Double, shape: ShapeCategory) {

        self.blackHoleMass = blackHoleMass
        self.stellarMass = stellarMass
        self.gasMass = mass - stellarMass - blackHoleMass
        self.shapeCategory = shape
 
        Galaxy.galaxyCount += 1
    }

    deinit {
        Galaxy.galaxyCount -= 1
        //Qalog("Deallocated a galaxy. \(Galaxy.galaxyCount) remaining.")
    }

    /// For debugging purposes, prints the name of the galaxy to the terminal.
    open func printGalaxy(_ name: String?) {

        if let name = name {
            print(name, terminator:" ")
        }
       //print("x: \(x), y: \(y), totalMass: \(totalMass), sfr: \(starFormationRate), stellarMass: \(stellarMass), gasMass: \(gasMass), \(self.findCategory().name())")

    }

    /**
     Sets the gas accretion rate to the specified fraction of the maximum rate.
     */
    open func updateGasAccretionRate(_ fraction: Double) {
        self.gasAccretionRate = fraction * self.gasAccretionRateMax
    }

    /**
     This function merges another galaxy into this one.
     After the merger, `otherGalaxy` is removed from the universe.

     - Parameter otherGalaxy: The other galaxy which is part of the interaction.
     
     */
    open func mergeWithGalaxy(_ otherGalaxy: Galaxy) {

        let galaxyType = otherGalaxy.category

        if !self.haveCollidedWithGasRichDisk || !self.haveCollidedWithGasPoorDisk || !self.haveCollidedWithGasPoorElliptical {
            switch galaxyType {
            case .heavyGasRichDisk:
                if !self.haveCollidedWithGasRichDisk {
                    self.haveCollidedWithGasRichDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasRichDisk), object: nil))
                }
            case .gasRichDisk:
                if !self.haveCollidedWithGasRichDisk {
                    self.haveCollidedWithGasRichDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasRichDisk), object: nil))
                }
            case .lightGasRichDisk:
                if !self.haveCollidedWithGasRichDisk {
                    self.haveCollidedWithGasRichDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasRichDisk), object: nil))
                }
            case .heavyGasPoorDisk:
                if !self.haveCollidedWithGasPoorDisk {
                    self.haveCollidedWithGasPoorDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorDisk), object: nil))
                }
            case .gasPoorDisk:
                if !self.haveCollidedWithGasPoorDisk {
                    self.haveCollidedWithGasPoorDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorDisk), object: nil))
                }
            case .lightGasPoorDisk:
                if !self.haveCollidedWithGasPoorDisk {
                    self.haveCollidedWithGasPoorDisk = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorDisk), object: nil))
                }
            case .heavyGasPoorElliptical:
                if !self.haveCollidedWithGasPoorElliptical {
                    self.haveCollidedWithGasPoorElliptical = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorElliptical), object: nil))
                }
            case .gasPoorElliptical:
                if !self.haveCollidedWithGasPoorElliptical {
                    self.haveCollidedWithGasPoorElliptical = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorElliptical), object: nil))
                }
            case .lightGasPoorElliptical:
                if !self.haveCollidedWithGasPoorElliptical {
                    self.haveCollidedWithGasPoorElliptical = true
                    NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.MergedWithGasPoorElliptical), object: nil))
                }
            default:
                break
            }
        }

        let gasCategory = gasConcentrationCategory()
        let otherGasCategory = otherGalaxy.gasConcentrationCategory()
        let massCategory = relativeMassCategory(otherGalaxy.totalMass)

        // First we add all the mass properties.
        self.stellarMass += otherGalaxy.stellarMass
        self.gasMass += otherGalaxy.gasMass
        self.blackHoleMass += otherGalaxy.blackHoleMass

        // Then set the other galaxy's mass properties to zero
        otherGalaxy.stellarMass = 0
        otherGalaxy.gasMass = 0
        otherGalaxy.blackHoleMass = 0

        // Adjustments needed for specific cases are taken care of below.
        switch gasCategory {
        case .poor: //user galaxy is gas poor (elliptical)

            switch otherGasCategory {
            case .poor: //other galaxy is also gas poor (elliptical)

                self.applyStarburst(0.0)

                switch massCategory {
                case .heavier: //other galaxy is heavier
                    self.shapeCategory = .elliptical
                case .equal: //other galaxy is equal in mass
                    self.shapeCategory = .elliptical
                case .lighter: //other galaxy is lighter
                    self.shapeCategory = .elliptical
                case .muchLighter: //other galaxy is much lighter
                    self.shapeCategory = .elliptical
                }

            case .rich: //other galaxy is gas rich (spiral)

                self.applyStarburst(0.05)

                switch massCategory {
                case .heavier: //other galaxy is heavier
                    self.shapeCategory = .elliptical
                case .equal: //other galaxy is equal in mass
                    self.shapeCategory = .elliptical
                case .lighter: //other galaxy is lighter
                    self.shapeCategory = .elliptical
                case .muchLighter: //other galaxy is much lighter
                    self.shapeCategory = .elliptical
                }
            }

        case .rich: //user galaxy is gas rich (spiral)

            switch otherGasCategory {
            case .poor: //other galaxy is gas poor (elliptical)

                self.applyStarburst(0.05)

                switch massCategory {
                case .heavier: //other galaxy is heavier
                    self.shapeCategory = .elliptical
                case .equal: //other galaxy is equal in mass
                    self.shapeCategory = .elliptical
                case .lighter: //other galaxy is lighter
                    self.shapeCategory = .elliptical
                case .muchLighter: //other galaxy is much lighter
                    self.shapeCategory = .spiral
                }


            case .rich: //other galaxy is also gas rich (spiral)

                self.applyStarburst(0.1)

                switch massCategory {
                case .heavier: //other galaxy is heavier
                    self.shapeCategory = .elliptical
                case .equal: //other galaxy is equal in mass
                    self.shapeCategory = .elliptical
                case .lighter: //other galaxy is lighter
                    self.shapeCategory = .spiral
                case .muchLighter: //other galaxy is much lighter
                    self.shapeCategory = .spiral
                }
            }
        }
        Universe.universe.removeGalaxy(otherGalaxy)
        Universe.universe.updateGalaxyProperties()
        NotificationCenter.default.post(Notification(name: Notification.Name(GENotification.Galaxy.GalaxyMerged), object: nil))
    }

    /**
     This function changes the galaxy properties according to interaction with another galaxy.

     - Parameter otherGalaxy: The other galaxy which is part of the interaction.
     */
    open func interactWithGalaxy(_ otherGalaxy: Galaxy) {

        let gasCategory = gasConcentrationCategory()
        let otherGasCategory = otherGalaxy.gasConcentrationCategory()
        //let massCategory = relativeMassCategory(otherGalaxy.totalMass)

        // Adjustments needed for specific cases are taken care of below.
        switch gasCategory {
        case .poor: //user galaxy is gas poor (elliptical)

            switch otherGasCategory {
            case .poor: //other galaxy is also gas poor (elliptical)
                self.applyStarburst(0.0)

            case .rich: //other galaxy is gas rich (spiral)
                self.applyStarburst(0.025)

            }

        case .rich: //user galaxy is gas rich (spiral)

            switch otherGasCategory {
            case .poor: //other galaxy is gas poor (elliptical)
                self.applyStarburst(0.025)

            case .rich: //other galaxy is also gas rich (spiral)
                self.applyStarburst(0.05)

            }
        }
        Universe.universe.removeGalaxy(otherGalaxy)
    }

    /**
      - gasConversionFraction: The fraction of gas that is converted into stellar mass during the starburst.
     */
    fileprivate func applyStarburst(_ gasConversionFraction: Double) {
        assert(gasConversionFraction >= 0)
        assert(gasConversionFraction <= 1)
        let starburstMassChange = gasConversionFraction * gasMass
        self.stellarMass += starburstMassChange
        self.gasMass -= starburstMassChange
    }

    fileprivate func updateStellarMass() {
        let delta = 10.0 //This will be the time delta between calls to this function
        self.stellarMass += self.starFormationRate * delta
    }
    
    // MARK: - Categories
    
    fileprivate func gasConcentrationCategory() -> GasConcentrationCategory {
        if gasLevel < gasThreshold {
            return .poor
        } else {
            return .rich
        }
    }
    
    fileprivate func relativeMassCategory(_ otherMass: Double) -> RelativeMassCategory {
        if otherMass > 3 * totalMass {
            return .heavier
        } else if otherMass < (1/3) * totalMass && otherMass > (1/10) * totalMass {
            return .lighter
        } else if otherMass < (1/10) * totalMass {
            return .muchLighter
        } else {
            return .equal
        }
    }

    fileprivate func absoluteMassCategory() -> AbsoluteMassCategory {
        // Absolute Mass ranges:
        let light = 1000.0
        let heavy = 100000.0

        if totalMass < light {
            return .light
        } else if totalMass < heavy {
            return .average
        } else {
            return .heavy
        }
    }

    /*func galacticNucleiCategory() -> GalacticNucleiCategory {
        //implement later
    }*/

    //func specialCategory() -> SpecialCategory {
    //}

    /**
     Returns the category of the galaxy.
     */
    fileprivate func findCategory() -> GalaxyType {

        switch self.absoluteMassCategory() {
        case .light:
            switch self.gasConcentrationCategory() {
            case .rich:
                return .lightGasRichDisk
            case .poor:
                switch self.shapeCategory {
                case .elliptical:
                    return .lightGasPoorElliptical
                case .spiral:
                    return .lightGasPoorDisk
                }
            }

        case .average:
            switch self.gasConcentrationCategory() {
            case .rich:
                return .gasRichDisk
            case .poor:
                switch self.shapeCategory {
                case .elliptical:
                    return .gasPoorElliptical
                case .spiral:
                    return .gasPoorDisk
                }
            }

        case .heavy:
            switch self.gasConcentrationCategory() {
            case .rich:
                return .heavyGasRichDisk
            case .poor:
                switch self.shapeCategory {
                case .elliptical:
                    return .heavyGasPoorElliptical
                case .spiral:
                    return .heavyGasPoorDisk
                }
            }

        }

        /*switch galaxy.absoluteMassCategory() {
        case .SuperHeavy:
            switch galaxy.gasConcentrationCategory() {
            case .Rich:
                return .SuperMassiveGasRichSpiral
            case .Poor:
                return .SuperMassiveGasPoorElliptical
            }
        case .Heavy:
            switch galaxy.gasConcentrationCategory() {
            case .Rich:
                return .MassiveGasRichSpiral
            case .Poor:
                return .MassiveGasPoorElliptical
            }
        case .Average:
            switch galaxy.gasConcentrationCategory() {
            case .Rich:
                return .NormalGasRichSpiral
            case .Poor:
                return .NormalGasPoorElliptical
            }
        case .Light:
            switch galaxy.gasConcentrationCategory() {
            case .Rich:
                return .LightGasRichSpiral
            case .Poor:
                return .LightGasPoorElliptical
            }
        case .VeryLight:
            switch galaxy.gasConcentrationCategory() {
            case .Rich:
                return .VeryLightGasRichSpiral
            case .Poor:
                return .VeryLightGasPoorElliptical
            }
        }*/
    }
    
}

















