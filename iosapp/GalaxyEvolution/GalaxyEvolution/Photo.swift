//
//  Photo.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/11/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Photo object for the gallery
open class Photo {
    open var name : String
    open var image : UIImage
    open var description = ""
    
    public init (name: String, image: UIImage, description: String) {
        self.name = name
        self.image = image
        self.description = description
    }
    
}
