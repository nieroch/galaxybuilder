//
//  LearnMoreManager.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 8/7/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//


import Foundation

// No longer used. The links were moved to thet Glossary./
open class LearnMoreManager {

    fileprivate var links : [(title: String, url: URL?)]

    /// The singleton instance of this class.
    static open let sharedInstance = LearnMoreManager()

    fileprivate init() {

        links = [("Hubble Galaxy Images", URL(string: "http://hubblesite.org/gallery/album/galaxy/")),
                 ("Hubble Site", URL(string: "http://hubblesite.org")),
                 ("Galaxy Mergers", URL(string: "http://candels-collaboration.blogspot.com/2012/06/cosmic-collisions-galaxy-mergers-and.html")),
                 ("Types of Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/06/what-types-of-galaxies-are-there.html")),
                 ("Galaxy Morphology", URL(string: "http://candels-collaboration.blogspot.com/2012/08/how-do-we-measure-galaxy-morphology.html")),
                 ("Galaxy Zoo Project", URL(string: "http://candels-collaboration.blogspot.com/2012/10/galaxy-zoo-meets-candels.html")),
                 ("Black Holes", URL(string: "http://candels-collaboration.blogspot.de/2012/06/supermassive-black-holes-and-active.html")),
                 ("Dead Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/08/what-turns-galaxies-off.html")),
                 ("Luminous Infrared Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/07/luminous-infrared-galaxies.html")),
                 ("The First Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2013/09/in-search-of-first-galaxies.html")),
                 ("End of the Dark Ages", URL(string: "http://candels-collaboration.blogspot.com/2012/11/the-clearing-of-cosmic-fog-and-end-of.html")),
                 ("Baby Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/07/baby-galaxies-in-distant-universe.html")),
                 ("Distant Galaxies", URL(string: "http://candels-collaboration.blogspot.com/2012/06/search-for-most-distant-galaxies.html")),
        ]

    }

    open func linksCount() -> Int {
        return links.count
    }

    open func linkAtIndex(_ index: Int) -> (title: String, url: URL?) {
        return links[index]
    }
}
