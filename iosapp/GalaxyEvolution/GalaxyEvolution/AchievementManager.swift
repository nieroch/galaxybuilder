//
//  AchievementManager.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/11/17.
//  Copyright © 2017 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// Manager class for the achievements
open class AchievementManager {

    fileprivate var achievements : [Achievement] = []

    fileprivate var images : [UIImage] = [UIImage(named: "Gas_Poor_Disk_Galaxy_2")!,
                                          UIImage(named: "Gas_Poor_Elliptical_2")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_2")!,
                                          UIImage(named: "Spiral_Galaxy_2")!,
                                          UIImage(named: "Gas_Poor_Disk_Galaxy_app_400")!,
                                          UIImage(named: "Gas_Poor_Elliptical_Galaxy_3")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_3")!,
                                          UIImage(named: "Test_Nebula_1")!,
                                          UIImage(named: "Gas_Poor_Elliptical_2")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_2")!,
                                          UIImage(named: "Spiral_Galaxy_2")!,
                                          UIImage(named: "Gas_Poor_Disk_Galaxy_2")!,
                                          UIImage(named: "Gas_Poor_Elliptical_2")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_2")!,
                                          UIImage(named: "Spiral_Galaxy_2")!,
                                          UIImage(named: "Gas_Poor_Disk_Galaxy_app_400")!,
                                          UIImage(named: "Gas_Poor_Elliptical_Galaxy_3")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_3")!,
                                          UIImage(named: "Test_Nebula_1")!,
                                          UIImage(named: "Gas_Poor_Elliptical_2")!,
                                          UIImage(named: "Gas_Rich_Disk_Galaxy_2")!,
                                          UIImage(named: "Spiral_Galaxy_2")!,
                                          ]

    fileprivate var descriptionTitles : [String] = ["Spiral Explorer",
                                                    "Black Hole Investiator",
                                                    "Cluster Creator",
                                                    "Elliptical Expert",
                                                    "Merger Master",
                                                    "Galaxy Guardian",
                                                    "Galaxy Guru",
                                                    "Achievement 8 Title",
                                                    "Achievement 9 Title",
                                                    "Achievement 10 Title",
                                                    "Achievement 11 Title",
                                                    "Achievement 12 Title",
                                                    "Achievement 13 Title",
                                                    "Achievement 14 Title",
                                                    "Achievement 15 Title",
                                                    "Achievement 16 Title",
                                                    "Achievement 17 Title",
                                                    "Achievement 18 Title",
                                                    "Achievement 19 Title",
                                                    "Achievement 20 Title",
                                                    "Achievement 21 Title",
                                                    "Achievement 22 Title"
                                                    ]

    fileprivate var descriptions : [String] = ["You’ve created a Milky Way type galaxy!",
                                               "You’ve created an AGN!",
                                               "You’ve created a massive elliptical galaxy in the center of a cluster!",
                                               "You’ve used up all your gas. No more stars can form in your galaxy.",
                                               "You’ve merged with 500 galaxies!",
                                               "You’ve avoided a merger with another galaxy!",
                                               "CONGRATUATIONS! You’ve created every type of galaxy!",
                                               "Achievement 8 description",
                                               "Achievement 9 description",
                                               "Achievement 10 description",
                                               "Achievement 11 description",
                                               "Cluster of galaxies",
                                               "Many galaxies",
                                               "Galaxies interacting",
                                               "Galaxies merging",
                                               "Achievement 16 description",
                                               "Achievement 17 description",
                                               "Achievement 18 description",
                                               "Achievement 19 description",
                                               "Achievement 20 description",
                                               "Achievement 21 description",
                                               "Achievement 22 description",
                                               ]

    fileprivate var states : [String] = ["Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         "Incomplete",
                                         "Complete",
                                         ]

    static let sharedInstance = AchievementManager()

    // Initializes the achievements array list that is used to set up the achievements page
    fileprivate init() {
        self.achievements.append(Achievement(name: "Spiral Explorer", image: images[0], description: descriptions[0], descriptionTitle: descriptionTitles[0], state: states[0]))
        self.achievements.append(Achievement(name: "Black Hole Investiator", image: images[1], description: descriptions[1], descriptionTitle: descriptionTitles[1], state: states[1]))
        self.achievements.append(Achievement(name: "Cluster Creator", image: images[2], description: descriptions[2], descriptionTitle: descriptionTitles[2], state: states[2]))
        self.achievements.append(Achievement(name: "Elliptical Expert", image: images[3], description: descriptions[3], descriptionTitle: descriptionTitles[3], state: states[3]))
        self.achievements.append(Achievement(name: "Merger Master", image: images[4], description: descriptions[4], descriptionTitle: descriptionTitles[4], state: states[4]))
        self.achievements.append(Achievement(name: "Galaxy Guardian", image: images[5], description: descriptions[5], descriptionTitle: descriptionTitles[5], state: states[5]))
        self.achievements.append(Achievement(name: "Galaxy Guru", image: images[6], description: descriptions[6], descriptionTitle: descriptionTitles[6], state: states[6]))
        self.achievements.append(Achievement(name: "Achievement 8", image: images[7], description: descriptions[7], descriptionTitle: descriptionTitles[7], state: states[7]))
        self.achievements.append(Achievement(name: "Achievement 9", image: images[8], description: descriptions[8], descriptionTitle: descriptionTitles[8], state: states[8]))
        self.achievements.append(Achievement(name: "Achievement 10", image: images[9], description: descriptions[9], descriptionTitle: descriptionTitles[9], state: states[9]))
        self.achievements.append(Achievement(name: "Achievement 11", image: images[10], description: descriptions[10], descriptionTitle: descriptionTitles[10], state: states[10]))
        self.achievements.append(Achievement(name: "Achievement 12", image: images[11], description: descriptions[11], descriptionTitle: descriptionTitles[11], state: states[11]))
        self.achievements.append(Achievement(name: "Achievement 13", image: images[12], description: descriptions[12], descriptionTitle: descriptionTitles[12], state: states[12]))
        self.achievements.append(Achievement(name: "Achievement 14", image: images[13], description: descriptions[13], descriptionTitle: descriptionTitles[13], state: states[13]))
        self.achievements.append(Achievement(name: "Achievement 15", image: images[14], description: descriptions[14], descriptionTitle: descriptionTitles[14], state: states[14]))
        self.achievements.append(Achievement(name: "Achievement 16", image: images[15], description: descriptions[15], descriptionTitle: descriptionTitles[15], state: states[15]))
        self.achievements.append(Achievement(name: "Achievement 17", image: images[16], description: descriptions[16], descriptionTitle: descriptionTitles[16], state: states[16]))
        self.achievements.append(Achievement(name: "Achievement 18", image: images[17], description: descriptions[17], descriptionTitle: descriptionTitles[17], state: states[17]))
        self.achievements.append(Achievement(name: "Achievement 19", image: images[18], description: descriptions[18], descriptionTitle: descriptionTitles[18], state: states[18]))
        self.achievements.append(Achievement(name: "Achievement 20", image: images[19], description: descriptions[19], descriptionTitle: descriptionTitles[19], state: states[19]))
        self.achievements.append(Achievement(name: "Achievement 21", image: images[20], description: descriptions[20], descriptionTitle: descriptionTitles[20], state: states[20]))
        self.achievements.append(Achievement(name: "Achievement 22", image: images[21], description: descriptions[21], descriptionTitle: descriptionTitles[21], state: states[21]))

    }

    open func achievementsList() -> [Achievement] {
        return achievements
    }
}
