//
//  Constants.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/30/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation

/**
 Notifications used to communicate from model to controllers.
 */
public struct GENotification {
    /// Notifications regarding universe.
    public struct Universe {
        /// Notification posted when the universe age has been modified.
        public static let AgeUpdated = "UniverseAgeUpdatedNotification"

        /// Notification posted when objects (galaxies) were add or removed from the universe. The `userInfo` object contains lists of the added and removed objects.
        public static let SpaceObjectsUpdated = "UniverseSpaceObjectsUpdatedNotification"

        /// Notification posted when the user galaxy has changed properties.
        public static let UserGalaxyCategoryChanged = "UniverseUserGalaxyCategoryChangedNotification"

        /// Notification posted when the universe evolution has been paused.
        public static let ObjectsPaused = "UniverseObjectsPausedNotification"

        /// Notification posted when the universe evolution has been resumed.
        public static let ObjectsResumed = "UniverseObjectsResumedNotification"
    }

    /// Notifications regarding the user galaxy.
    public struct Galaxy {
        /// Notification posted when the user galaxy merged with another galaxy.
        public static let GalaxyMerged = "UserGalaxyMergedNotification"

        /// Notification posted when the user galaxy merged with gas rich disk galaxy.
        public static let MergedWithGasRichDisk = "GalaxyMergedWithGasRichDiskNotification"

        /// Notification posted when the user galaxy merged with gas poor disk galaxy.
        public static let MergedWithGasPoorDisk = "GalaxyMergedWithGasPoorDiskNotification"

        /// Notification posted when the user galaxy merged with gas poor elliptical galaxy.
        public static let MergedWithGasPoorElliptical = "GalaxyMergedWithGasPoorEllipticalNotification"
    }
}

public struct GEKeys {
    public static let AddedSpaceObjectsKey = "AddedSpaceObjects"
    public static let RemovedSpaceObjectsKey = "RemovedSpaceObjects"
}





