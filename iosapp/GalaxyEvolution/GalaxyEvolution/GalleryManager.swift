//
//  GalleryManager.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/11/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation
import UIKit

// This class holds the information used to set up the gallery
open class GalleryManager {

    fileprivate var photos : [Photo] = []
    fileprivate var descriptions : [String] = ["Cluster of galaxies",
                                           "Many galaxies",
                                           "Galaxies interacting",
                                           "Galaxies merging",
                                           "    This is a spectacular image taken by the NASA Hubble Space Telescope of the disk galaxy NGC 5866. What makes this image unique is that the galaxy is tilted so we see its edge. The black line is lane of dust running through the disk of the galaxy. A blue disk of stars runs parallel to the dust, a bulge surrounds the nucleus, and there is a faint, translucent halo encompassing the entire galaxy. Globular clusters, clusters of millions of stars each, also surround the galaxy, visible as small dots in the halo.\n  NGC 5866 is a type \"S0\" galaxy, meaning that it would not have a defined spiral structure if we viewed it head on. However, because it is very flat, it is still considered a disk galaxy instead of an elliptical galaxy. This galaxy lies about 44 million light-years away in the Draco constellation. Its diameter is only two-thids that of the Milky Way's.",
                                           "Galaxy M106",
                                           "    This beautiful image by the NASA Hubble Space Telescope is of a galaxy Messier 64 (M64). This galaxy has a massive, dark band of light absorbing dust that lies in front of the nucleus, hence giving the galaxy the nickname \"Black Eye\" or \"Evil Eye\" galaxy.\n   This galaxy is familiar to many amateur astronomers because it is visible even when using a small telescope. It was first cataloged by French astronomer Messier in the 1700's. It is about 17 million light-years away from Earth in the Coma Berenices constellation. M64 is quite unique because gas in its outer regions rotates in the opposite direction from stars and gas in the galaxy's inner regions. Astronomers hypothesize that this strange phenomena arose when a smaller galaxy collided with M64, possibly more than one billion years ago.",
                                           "Galactic starburst",
                                           "The Sombrero Galaxy",
                                           "The Whirlpool Galaxy",
                                           "A barred spial galaxy",
                                           "The Cartwheel Galaxy",
                                           "A distant black hole",
                                           "A beautifully dusty galaxy",
                                           "More galaxies interacting",
                                           "Even more galaxies interacting",
                                           "Two galaxies lined up. The large one in the back illuminates the dark dust clouds of the one in front.",
                                           "The nucleus of a galaxy",
                                           "A triplet of galaxies",
                                           "Haog's Object Galaxy",
                                           "Magellanic gemstones",
                                           "A quasar at the core of a galaxy",
                                           "The Small Magellanic Cloud, which is a small galaxy in the vicinity of the Milky Way.",
                                           "The halo of a beautiful supernova",
                                           "    This image is of galaxy UGC 10214, also known as the Tadpole Galaxy due to its \"tail\". Its unusual shape is thought to be caused by its interaction with the small, blue galaxy visible in the top left corner of UGC 10214. This is a classic example of a galaxy interacting with another galaxy, but not merging. Sometimes, as in this case, small galaxies can be drawn near a larger galaxy and tehn pass by. Although the small galaxy does not entirely merge with the large galaxy, the garvitational effects of the interaction can still cause distortions in shape, such as the tail of the Tadpole Galaxy. One can spot clusters of young, blue stars near the site of interaction. These stars were formed as a result of the interaction and they are blue because they are far hotter and brighter than our Sun. The Tadpole Galaxy resides in the Draco constellation, about 420 million light-years away.",
                                           "A spectacularly yellow spiral galaxy",

    ]

    fileprivate var videos : [UIView] = []
    fileprivate var videoDescriptions : [String] = ["This is a simlulation of a galactic merger. The video pauses the simulation at certain points and compares it to real pictures taken of galactic mergers."
    ]

    static let sharedInstance = GalleryManager()

    // Initializes the photos array list to contain all the images we want in the gallery
    fileprivate init() {
        photos.append(Photo(name: "Galaxy Cluster", image: UIImage(named: "Galaxy_Cluster")!, description: descriptions[0]))
        photos.append(Photo(name: "Candels Image", image: UIImage(named: "candels_background")!, description: descriptions[1]))
        photos.append(Photo(name: "Galaxy Interaction", image: UIImage(named: "Galaxies_Interacting")!, description: descriptions[2]))
        photos.append(Photo(name: "Galaxy Merger", image: UIImage(named: "hubble-galaxy-merger")!, description: descriptions[3]))
        photos.append(Photo(name: "Galaxy Edge", image: UIImage(named: "Galaxy_Edge")!, description: descriptions[4]))
        photos.append(Photo(name: "Galaxy M106", image: UIImage(named: "Galaxy_M106")!, description: descriptions[5]))
        photos.append(Photo(name: "Black Eye Galaxy", image: UIImage(named: "Galaxy_Merger_BlackEye")!, description: descriptions[6]))
        photos.append(Photo(name: "Starburst", image: UIImage(named: "Galaxy_Starburst")!, description: descriptions[7]))
        photos.append(Photo(name: "Sombrero Galaxy", image: UIImage(named: "Sombrero_Galaxy")!, description: descriptions[8]))
        photos.append(Photo(name: "Whirlpool Galaxy", image: UIImage(named: "Whirlpool_Galaxy")!, description: descriptions[9]))
        photos.append(Photo(name: "Barred Spiral Galaxy", image: UIImage(named: "Barred_Spiral_Galaxy")!, description: descriptions[10]))
        photos.append(Photo(name: "Cartwheel Galaxy", image: UIImage(named: "Cartwheel_Galaxy")!, description: descriptions[11]))
        photos.append(Photo(name: "Distant Black Hole", image: UIImage(named: "Distant_Black_Hole")!, description: descriptions[12]))
        photos.append(Photo(name: "Dusty Galaxy", image: UIImage(named: "Dusty_Galaxy")!, description: descriptions[13]))
        photos.append(Photo(name: "Galaxy Interaction 2", image: UIImage(named: "Galaxies_Interacting_2")!, description: descriptions[14]))
        photos.append(Photo(name: "Galaxy Interaction 3", image: UIImage(named: "Galaxies_Interacting_3")!, description: descriptions[15]))
        photos.append(Photo(name: "Lined Up Galaxies", image: UIImage(named: "Galaxies_Lined_Up")!, description: descriptions[16]))
        photos.append(Photo(name: "Galaxy Nucleus", image: UIImage(named: "Galaxy_Nucleus")!, description: descriptions[17]))
        photos.append(Photo(name: "Galactic Triplets", image: UIImage(named: "Galaxy_Triplets")!, description: descriptions[18]))
        photos.append(Photo(name: "Haog's Object Galaxy", image: UIImage(named: "Haog's_Object_Galaxy")!, description: descriptions[19]))
        photos.append(Photo(name: "Magellanic Gemstones", image: UIImage(named: "Magellanic_Gemstones")!, description: descriptions[20]))
        photos.append(Photo(name: "Quasar at Galactic Core", image: UIImage(named: "Quasar_Core")!, description: descriptions[21]))
        photos.append(Photo(name: "Small Magellanic Cloud", image: UIImage(named: "Small_Magellanic_Cloud")!, description: descriptions[22]))
        photos.append(Photo(name: "Supernova Halo", image: UIImage(named: "Supernova_Halo")!, description: descriptions[23]))
        photos.append(Photo(name: "Tadpole Galaxy", image: UIImage(named: "Tadpole_Galaxy")!, description: descriptions[24]))
        photos.append(Photo(name: "Golden Spiral", image: UIImage(named: "Yellow_Spiral")!, description: descriptions[25]))

        //videos.append(VideoView(name: "Galatic Merger Simulation", description: videoDescriptions[0], type: "m4v"))
    }

    open func photoList() -> [Photo] {
        return photos
    }

    open func videoList() -> [UIView] {
        return videos
    }
}
