//
//  GameScene.swift
//  SpriteKitAccelerometerTutorial
//
//  Created by Niels Rasmussen on 7/7/16.
//  Copyright (c) 2016 Niels Rasmussen. All rights reserved.
//

import SpriteKit
import CoreMotion

//let background1 = SKSpriteNode(imageNamed: "candels_background.jpg")
//let background2 = SKSpriteNode(imageNamed: "candels_background.jpg")

// GameScene mangers much of the runtime code for the game. Deals with the sprites and the collisions.
class GameScene: SKScene, SKPhysicsContactDelegate {
    
    //var gameSceneDelegate : UpdateGasLabelDelegate?
    
    let spaceObjectCategory : UInt32 = 0x1 << 0
    let userObjectCategory : UInt32 = 0x1 << 1

    var velocityX : CGFloat = 0.0 // units: points per second
    var velocityY : CGFloat = 0.0 // units: points per second
    var dataX : CGFloat = 0.0
    var dataY : CGFloat = 0.0
    
    var previousTime : CFTimeInterval?
    var spawnTime : CFTimeInterval = 0.0
    let spriteBallScale : CGFloat = 0.35
    var galaxies : [SKNode : Galaxy] = [:]

    let cameraNode = SKCameraNode()
    var zoomFactor : CGFloat = 1.0
    let enableZoom : Bool = false

    /*
    override init() {
        
        //super.init(size:CGSizeMake(768, 1024))
        super.init(size:CGSize(width: (UIApplication.shared.keyWindow?.bounds.width)!, height: (UIApplication.shared.keyWindow?.bounds.height)!))
        
        self.backgroundColor = UIColor.init(white: 0.0, alpha: 1)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */

        NotificationCenter.default.addObserver(self, selector: #selector(GameScene.handleSpaceObjectUpdate(_:)), name: NSNotification.Name(GENotification.Universe.SpaceObjectsUpdated), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameScene.updateUserSpriteImage(_:)), name: NSNotification.Name(GENotification.Universe.UserGalaxyCategoryChanged), object: nil)

        self.physicsWorld.contactDelegate = self
        self.setupUserSprite()

        if enableZoom {
            cameraNode.position = CGPoint(x: 0, y: 0)
            scene?.addChild(cameraNode)
            scene?.camera = cameraNode
        }
        
    }

    override func willMove(from view: SKView) {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Zoom

    func zoomIn() {
        self.zoomFactor /= 2.0
        let zoomInAction = SKAction.scale(to: zoomFactor, duration: 1)
        cameraNode.run(zoomInAction)
    }

    func zoomOut() {
        self.zoomFactor *= 2.0
        let zoomInAction = SKAction.scale(to: zoomFactor, duration: 1)
        cameraNode.run(zoomInAction)
    }

    func resetScene() {
        self.previousTime = nil
    }

    // MARK: - Utilities

    fileprivate func rotateActionForImage(_ imageName:String, duration:TimeInterval) -> SKAction? {
        switch ImageManager.rotationDirection(imageName) {
        case .clockwise:
            return SKAction.rotate(byAngle: CGFloat(-Double.pi), duration:duration)
        case .counterClockwise:
            return SKAction.rotate(byAngle: CGFloat(Double.pi), duration:duration)
        case .none:
            return nil
        }
    }


    // MARK: -

    func handleSpaceObjectUpdate(_ notification: Notification) {
        if let removed = (notification as NSNotification).userInfo?[GEKeys.RemovedSpaceObjectsKey] as? [Galaxy] {
            Qalog("Handling the \(removed.count) removed galaxies")

            for galaxy in removed {
                if let sprite = galaxy.sprite as? SKSpriteNode {
                    sprite.removeFromParent()
                    galaxy.sprite = nil
                }
            }

        }

        if let added = (notification as NSNotification).userInfo?[GEKeys.AddedSpaceObjectsKey] as? [Galaxy] {
            Qalog("Handling the \(added.count) added galaxies")

            for galaxy in added {
                let image  = ImageManager.imageForGalaxy(galaxy, applyColorTint: false)
                let sprite = SKSpriteNode(texture: SKTexture(image: image))
                galaxy.sprite = sprite
                galaxies[sprite] = galaxy
                sprite.position = CGPoint(x: CGFloat(galaxy.x), y: CGFloat(galaxy.y))
                sprite.name = "non user-galaxy"

                sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.frame.width/2.0)
                sprite.physicsBody!.isDynamic = false
                sprite.physicsBody!.affectedByGravity = false
                sprite.physicsBody!.categoryBitMask = spaceObjectCategory
                sprite.physicsBody!.contactTestBitMask = userObjectCategory
                sprite.physicsBody!.collisionBitMask = 0

                sprite.setScale(self.imageScale(galaxy.category))    //FIXME: scale should be calculated based on galaxy radius (converting lightyears to points)
                addChild(sprite)
                if let action = rotateActionForImage(galaxy.imageName, duration: 10) {
                    sprite.run(SKAction.repeatForever(action))
                }
            }

        }
    }

    func imageScale(_ type: Galaxy.GalaxyType) -> CGFloat {
        switch type {
        case .heavyGasRichDisk:
            return CGFloat(0.30)
        case .gasRichDisk:
            return CGFloat(0.35)
        case .gasPoorDisk:
            return CGFloat(0.27)
        case .lightGasPoorDisk:
            return CGFloat(0.27)
        case .heavyGasPoorElliptical:
            return CGFloat(0.30)
        case .gasPoorElliptical:
            return CGFloat(0.30)
        case .lightGasPoorElliptical:
            return CGFloat(0.30)
        default:
            return CGFloat(0.17)
        }
    }

    func userImageScale(_ type: Galaxy.GalaxyType) -> CGFloat {
        switch type {
        case .heavyGasRichDisk:
            return CGFloat(0.45)
        case .gasRichDisk:
            return CGFloat(0.53)
        case .lightGasRichDisk:
            return CGFloat(0.25)
        case .heavyGasPoorDisk:
            return CGFloat(0.50)
        case .gasPoorDisk:
            return CGFloat(0.32)
        case .lightGasPoorDisk:
            return CGFloat(0.32)
        case .heavyGasPoorElliptical:
            return CGFloat(0.24)
        case .gasPoorElliptical:
            return CGFloat(0.24)
        case .lightGasPoorElliptical:
            return CGFloat(0.20)
        default:
            return CGFloat(0.18)
        }
    }

    //@objc protocol UpdateGasLabelDelegate {
    //    func updateGasLabelDelegateFunc(level: Double)
    //}
    
    override func didChangeSize(_ oldSize: CGSize) {
        //Qalog("\(self.size) \(self.frame)")
    }

    func setValues(_ accelX: Double, accelY: Double) {
        let adjustment = sin(0.0 / 180.0 * Double.pi)
        let scale = 750.0

        self.velocityX = CGFloat(accelX * scale)
        if self.velocityX > CGFloat(Universe.universe.universalSpeedLimit) {
            self.velocityX = CGFloat(Universe.universe.universalSpeedLimit)
        } else if self.velocityX < -CGFloat(Universe.universe.universalSpeedLimit) {
            self.velocityX = -CGFloat(Universe.universe.universalSpeedLimit)
        }

        self.velocityY = CGFloat((accelY + adjustment) * scale)
        if self.velocityY > CGFloat(Universe.universe.universalSpeedLimit) {
            self.velocityY = CGFloat(Universe.universe.universalSpeedLimit)
        } else if self.velocityY < -CGFloat(Universe.universe.universalSpeedLimit) {
            self.velocityY = -CGFloat(Universe.universe.universalSpeedLimit)
        }

        self.dataX = CGFloat(accelX)
        self.dataY = CGFloat(accelY)
    }

    func didBegin(_ contact: SKPhysicsContact) {

        if contact.bodyA.categoryBitMask & spaceObjectCategory != 0 {
            if let galaxy = galaxies[(contact.bodyA.node as? SKSpriteNode)!] {
                Universe.universe.userGalaxy.mergeWithGalaxy(galaxy)
            }
            contact.bodyA.node?.removeFromParent()
        } else if contact.bodyB.categoryBitMask & spaceObjectCategory != 0 {
            if let galaxy = galaxies[(contact.bodyB.node as? SKSpriteNode)!] {
                Universe.universe.userGalaxy.mergeWithGalaxy(galaxy)
            }
            contact.bodyB.node?.removeFromParent()
        }
        Qalog()
    }

    func setupUserSprite() {
        let image  = ImageManager.imageForGalaxy(Universe.universe.userGalaxy, applyColorTint: false)
        let sprite = SKSpriteNode(texture: SKTexture(image: image))

        Universe.universe.userGalaxy.sprite = sprite

        sprite.position = CGPoint(x: CGFloat(Universe.universe.userGalaxy.x), y: CGFloat(Universe.universe.userGalaxy.y))

        sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.frame.width/2.0)
        sprite.physicsBody!.isDynamic = true
        sprite.physicsBody!.affectedByGravity = false
        sprite.physicsBody!.categoryBitMask = userObjectCategory
        sprite.physicsBody!.contactTestBitMask = spaceObjectCategory
        sprite.physicsBody!.collisionBitMask = 0
        
        sprite.setScale(self.userImageScale(Universe.universe.userGalaxy.category))     //FIXME: scale should be calculated based on galaxy radius (lightyears to points)
        addChild(sprite)
        
        if let action = rotateActionForImage(Universe.universe.userGalaxy.imageName, duration: 15) {
            sprite.run(SKAction.repeatForever(action))
        }

    }

    func updateUserSpriteImage(_ notification: Notification) {
        if let sprite = Universe.universe.userGalaxy.sprite as? SKSpriteNode {
            let image  = ImageManager.imageForGalaxy(Universe.universe.userGalaxy, applyColorTint: false)
            sprite.texture = SKTexture(image: image)
            sprite.setScale(self.userImageScale(Universe.universe.userGalaxy.category))
        }
    }

    // Important function that changes the position of the user galaxy and the user view to make it look like the user galaxy is moving
    override func update(_ currentTime : TimeInterval) {
        if let lastTime = previousTime {
            //NSLog("vX: \(velocityX) dx: \(dx) ∆t: \(timeDelta)")
            //Qalog("\(timeDelta)  \(dx)  \(dy) speed = \(sqrt(dx*dx + dy*dy)/CGFloat(timeDelta)) points/sec")
            //self.backgroudScrollUpdate()

            let timeDelta = currentTime - lastTime

            let dx = velocityX * CGFloat(timeDelta)
            let dy = velocityY * CGFloat(timeDelta)


            let duration = 0.0

            if let sprite = Universe.universe.userGalaxy.sprite as? SKSpriteNode {
                Universe.universe.userGalaxy.x += Float(dx)
                Universe.universe.userGalaxy.y += Float(dy)

                let newPosition = CGPoint(x: CGFloat(Universe.universe.userGalaxy.x), y: CGFloat(Universe.universe.userGalaxy.y))
                sprite.run(SKAction.move(to: newPosition, duration: duration))

                if enableZoom {
                    scene?.camera?.position = newPosition
                }

                // as the user galaxy moves, move the anchor point by an opposite amount, so that the user galaxy appears to stay still in the center:
                self.anchorPoint.x = 0.5 - sprite.position.x/frame.size.width
                self.anchorPoint.y = 0.5 - sprite.position.y/frame.size.height
            }
        }
        previousTime = currentTime
    }
    
    /*override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        //let touch : UITouch = touches.anyObject() as UITouch
        let positionInScene = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(positionInScene)

        if let name = touchedNode.name
        {
            if name == "pineapple"
            {
                print("Touched")
            }
        }
    }*/
    
}














