//
//  GasTank.swift
//  GalaxyEvolution
//
//  Created by Niels Rasmussen on 7/15/16.
//  Copyright © 2016 Niels Rasmussen. All rights reserved.
//

import Foundation

// Gas tank objerct used to update the gas level shown to the user
class GasTank {
    
    var currentLevel : Double
    var maxLevel : Double
    
    init(current: Double, max: Double) {
        currentLevel = current
        maxLevel = max
    }
    
    func getCurrentLevel() -> Double {
        return currentLevel
    }
    
    func getMaxLevel() -> Double {
        return maxLevel
    }
    
    func changeCurrentLevel(_ change: Double) {
        currentLevel += change
        //NSLog("\(self.currentLevel)")
    }
    
    func changeMaxLevel(_ change: Double) {
        maxLevel += change
    }    
}
